
#include "LoginServer.h"
#include <iostream>

#include <thread>
#include <chrono>


void LookForInput(LoginServer* atest)
{
	char test;
	std::cin >> test;
	atest->mbRunning = false;
}



void main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	srand((time(NULL)));
	LoginServer* mServer = new LoginServer();
	mServer->InitializeDatabase();

	std::thread thread(LookForInput, mServer);
	mServer->Initialize();
	mServer->Run();
	
	thread.join();
	delete mServer;
	mServer = nullptr;

}
