#include "LoginNetworkClient.h"
#include "GameMessages.h"
#include <iostream>
#include <thread>
#include <chrono>

LoginNetworkClient::LoginNetworkClient()
{
	mbConnected = false;
	m_pPeerInterface = nullptr;
	LoggedIn = false;
	mbConnectedToMain = false;

	mGameObject.mvPosition = glm::vec4(0);
}


LoginNetworkClient::~LoginNetworkClient()
{

}

void LoginNetworkClient::SetMainServer(char * aIP, int aPORT)
{
	IP = aIP;
	PORT = aPORT;
}

void LoginNetworkClient::handleNetworkMessages()
{
	RakNet::Packet* packet;
	for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
	{
		switch (packet->data[0])
		{
		case ID_INVALID_USERPASS:
			onRejectedLogin(packet);
			break;
		case ID_USERNAME_EXISTS:
			onRejectedRegistration(packet);
			break;
		case ID_USERPASS_ACCEPTED:
			std::cout << "Authenticated" << std::endl;
			onAcceptedLogin(packet);
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			std::cout << "Another client has disconnected.\n";
			onRemoteDisconnection(packet);
			break;
		case ID_REMOTE_CONNECTION_LOST:
			std::cout << "Another client has lost the connection.\n";
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			//receiving a remote connection
			onReceivedClientConnections(packet);
			break;
		case ID_REMOTE_CONNECTED_CLIENTS:
			onReceiveConnectedClients(packet);
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			onAcceptedConnection(packet);
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			std::cout << "The server is full.\n";
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			std::cout << "We have been disconnected.\n";
			LoggedIn = false;
			break;
		case ID_CONNECTION_LOST:
			std::cout << "Connection lost.\n";
			onLostConnection(packet);
			break;
		case ID_SERVER_SET_CLIENT_ID:
			//handle incoming setting of client id this is only done if their token was accepted
			onSetClientIDPacket(packet);
			break;
		case ID_LSERVER_ALREADY_LOGGED_IN:
			std::cout << "This user was already logged in " << std::endl;
			//Attempt to remotely disconnect the player
			{
				RakNet::BitStream bs;
				bs.Write((RakNet::MessageID)ID_CLIENT_REMOTE_LOGOUT);
				bs.Write(mUser.username);
				m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true); //send the packet containing my username
			}
			break;
		case ID_CLIENT_LOG_OUT:
			std::cout << "Client is logging out " << std::endl;
			onClientLogOut(packet);
			break;
		case ID_INVALID_TOKEN:
			std::cout << "Server said invalid token " << std::endl;
			mbConnectedToMain = false;
			LoggedIn = false;
			break;
		case ID_MAIN_SET_ID:
			onSetClientIDPacket(packet);
			serverSetObject();
			break;
		case ID_CLIENT_CLIENT_DATA:
			OnReceivedClientDataPacket(packet);
			break;
		default:
			break;
		}
	} //end of for loop

}

void LoginNetworkClient::ConnectToMain()
{
	std::cout << "Connecting to the main server" << std::endl;
	CreateConnection(IP, PORT);
	mbConnectedToMain = true;
}

void LoginNetworkClient::CheckForLogIn()
{
	if (IsLoggedIn() && !ConnectedToMain())
	{
		//do things
		std::cout << "Disconnecting from the login server" << std::endl;
		Disconnect();
		ConnectToMain();
	}
}

void LoginNetworkClient::SetLoginIP(char * IP, int PORT)
{
	LoginIP = IP;
	loginPORT = PORT;
}

void LoginNetworkClient::onRegistrationAttempt(User a_user)
{
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_CLIENT_REGISTER);
	bs.Write(a_user);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void LoginNetworkClient::onLoginAttempt(User a_user)
{
	//copy the username from argument into mUser struct so I know who I am
	strcpy_s(mUser.username, a_user.username);
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_CLIENT_LOGIN);
	bs.Write(a_user);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true); //send the packet containing my username
}

void LoginNetworkClient::onClientLogOut(RakNet::Packet * packet)
{
	//find the username in the packet and delete them.
	RakNet::RakString packetdata;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetdata);

}

void LoginNetworkClient::onRejectedRegistration(RakNet::Packet * packet)
{
	std::cout << "Registration rejected" << std::endl;
}

void LoginNetworkClient::onRejectedLogin(RakNet::Packet * packet)
{
	std::cout << "Login rejected, invalid username or password" << std::endl;
}

void LoginNetworkClient::onAcceptedLogin(RakNet::Packet * packet)
{
	std::cout << "Username and password accepted" << std::endl;

	RakNet::RakString testToken;
	strcpy_s(mLoginInfo.username, mUser.username);

	std::cout << "Reading in the authentication token" << std::endl;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(testToken);

	//set my token
	strcpy_s(mLoginInfo.token, testToken.C_String());
	std::cout << mLoginInfo.token << std::endl;
	//set the login token

	//set logged in to true to connect to main server
	LoggedIn = true;
}

void LoginNetworkClient::onAcceptedConnection(RakNet::Packet * packet)
{
	if (mbConnectedToMain)
	{
		std::cout << "Connected to the main server " << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_CLIENT_TOKEN);
		bs.Write(mLoginInfo);
		//send only to the server
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);

	}
	else
	{
		std::cout << "Connected to something else" << std::endl;
		//don't do the things
	}
}

void LoginNetworkClient::serverSetObject()
{
	//setup a bitstream 
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_CLIENT_SET_DATA);
	//write the gameobject to send to server
	bsOut.Write(mGameObject);
	bsOut.Write(miClientID);
	//send the client id and gameobject
	//send my data
	m_pPeerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void LoginNetworkClient::OnReceivedClientDataPacket(RakNet::Packet * packet)
{
	//whenever receiving other client data.
	GameObject temp;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	int clientID;
	bsIn.Read(clientID);
	bsIn.Read(temp);

	if (clientID != miClientID)
	{
		mClientGameObjects[clientID].mvPosition = temp.mvPosition;
	}
}

void LoginNetworkClient::onReceivedClientConnections(RakNet::Packet * packet)
{
	GameObject temp;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

	int clientID;
	bsIn.Read(clientID);
	bsIn.Read(temp);

	//load any new players
	if (clientID != miClientID)
	{
		std::cout << "Another client of ID: " << clientID << " Just connected" << std::endl;
		//load a new incoming player
		mClientGameObjects[clientID].mvPosition = temp.mvPosition;
	}
}

void LoginNetworkClient::onReceiveConnectedClients(RakNet::Packet * packet)
{
	GameObject temp;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	int clientID;
	bsIn.Read(clientID);
	bsIn.IgnoreBytes(sizeof(int));
	bsIn.Read(temp);

	std::cout << "Loading already connected players" << std::endl;

	if (clientID != miClientID)
	{
		mClientGameObjects[clientID].mvPosition = temp.mvPosition;
	}
}

void LoginNetworkClient::onRemoteDisconnection(RakNet::Packet * packet)
{
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	int clientID;
	bsIn.Read(clientID);
	std::cout << clientID << "Just disconnected" << std::endl;

	mClientGameObjects.erase(clientID);
}

void LoginNetworkClient::LogOut()
{
	mbConnected = false;
	std::cout << "telling the server im logging out " << std::endl;
	std::cout << mUser.username << std::endl;
	RakNet::BitStream bsL;
	bsL.Write((RakNet::MessageID)ID_CLIENT_LOG_OUT);
	bsL.Write(mUser.username);
	//tell everything im connected to I am logging out.
	m_pPeerInterface->Send(&bsL, MEDIUM_PRIORITY, RELIABLE, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	//tell the client im disconnecting
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_DISCONNECTION_NOTIFICATION);
	bs.Write("hello");
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	//std::cout << "Client disconnecting" << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	RakNet::SystemAddress temp(IP, PORT);
	m_pPeerInterface->CloseConnection(temp, true);

	delete m_pPeerInterface;
	m_pPeerInterface = nullptr;

	mbConnected = false;

	//re connect to the main.
	mbConnectedToMain = false;
	LoggedIn = false;

}

void LoginNetworkClient::DisconnectLogOut()
{
	mbConnected = false;

	if (LoggedIn && mbConnectedToMain) 
	{
		std::cout << "telling the server im logging out " << std::endl;
		RakNet::BitStream bsL;
		bsL.Write((RakNet::MessageID)ID_CLIENT_LOG_OUT);
		bsL.Write(mUser.username);
		//tell everything im connected to I am logging out.
		m_pPeerInterface->Send(&bsL, LOW_PRIORITY, RELIABLE, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}
	//tell the client im disconnecting
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_DISCONNECTION_NOTIFICATION);
	bs.Write("hello");
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	//std::cout << "Client disconnecting" << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	mbConnected = false;

	RakNet::SystemAddress temp(IP, PORT);
	m_pPeerInterface->CloseConnection(temp, true);

	delete m_pPeerInterface;
	m_pPeerInterface = nullptr;

	mbConnected = false;
}

void LoginNetworkClient::UpdateObject()
{
	RakNet::BitStream bs;

	bs.Write((RakNet::MessageID)ID_CLIENT_CLIENT_DATA);
	bs.Write(miClientID);
	bs.Write(mGameObject);
	m_pPeerInterface->Send(&bs, LOW_PRIORITY, RELIABLE, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

bool LoginNetworkClient::IsLoggedIn()
{
	return LoggedIn;
}

bool LoginNetworkClient::ConnectedToMain()
{
	return mbConnectedToMain;
}

void LoginNetworkClient::ConnectToLogin()
{
	CreateConnection(LoginIP, loginPORT);

	RakNet::BitStream bs;
	std::cout << "sendin my username incase i wans't logged out" << std::endl;
	bs.Write((RakNet::MessageID)ID_SERVER_CLIENT_LOGOUT_NOTIFICATION);
	bs.Write(mUser.username);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, RakNet::SystemAddress(LoginIP, loginPORT), false);
}

