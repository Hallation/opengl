#include "LoginServer.h"
#include "GameMessages.h"


#include <DependentExtensions\SQLite3Plugin\sqlite3.h>
#include <cryptlib.h>

#include <osrng.h>
#include <hex.h>
#include <sha.h>
#include <iostream>
#include <sstream>

LoginServer::LoginServer()
{
	RCONPassword = "password123";
	miMaxClients = 12;
	PORT = 9999;
	mbRunning = true;
	m_pPeerInterface = nullptr;
}


LoginServer::~LoginServer()
{
	if (m_pPeerInterface != nullptr)
	{
		delete m_pPeerInterface;
		m_pPeerInterface = nullptr;
	}
}


void LoginServer::InitializeDatabase()
{
	//make a default database
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		//initialize sqlite3
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string createTableSQL = "CREATE TABLE IF NOT EXISTS user (userID INT PRIMARY KEY, username TEXT, password TEXT, userSalt TEXT);";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, createTableSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare create table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to create table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}

		//if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, "INSERT IGNORE INTO user (userID, username, password, usersalt, loginCount) VALUES (00002, 'badusername', 'badpassword', 'badsalt', 1);", -1, &query, NULL)))
		//{
		//	printf("Failed to insert into table\n");
		//	break;
		//}
		//else if (sqlite3_step(query) != SQLITE_DONE)
		//{
		//	printf("Failed to insert into table %d, %s\n", ret, sqlite3_errmsg(pDB));
		//	break;
		//}
		break;
	}

	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();
}

void LoginServer::Insert(char * username, char * password)
{
	/*
	All checking to see if the data can be inserted has been processed before hand
	Before inserting, we need to hash the given password and require a salt to be generated;

	Count how many rows in db to know what our last ID is to get unique user IDS.
	From last user ID, using id formatting (0001....) iterate from last number
	Set this to be a new user entry.

	Select username from databse, if the username doesn't exist, they are good to go, otherwise reject anything further
	(probably do this before insert is ever used.
	Generate salt.
	Hash password with salt

	Insert hashed salted password into db

	insert salt into db
	*/
	int userID = (FindNextID() + 1);
	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	//username if it exists already done before enterting this method
	std::string salt = CreateSalt();
	std::string hashedPassword = HashPassword(password, salt);


	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}
		//compile SQL
		//table structure, userID INT PRIMARY KEY, username TEXT, password TEXT, userSalt TEXT, loginCount INT)
		std::ostringstream sstream;
		//password is hashed with salt.
		sstream << "INSERT INTO user (userID, username, password, userSalt) VALUES (" << userID << ",'" << username << "','" << hashedPassword << "','" << salt << "');";
		std::string sql = sstream.str();
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, sql.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare insert into table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to insert item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		break;
	}
	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();
}

bool LoginServer::AuthenticateUser(char * username, char * password)
{
	std::cout << "Authenticating the user" << std::endl;
	bool output = false;
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
			break;
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
			break;
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		//0 ID, 1 usr, 2 pwd, 3 salt, 4 count

		std::string count = " SELECT count(*) as COUNT, userID FROM user where username = '" + std::string(username) + "';";

		/*
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, count.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare count table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		else if (sqlite3_step(query) != SQLITE_ROW)
		{
			printf("Faield to count rows: %d, %s\n", ret, sqlite3_errmsg(pDB));
		}
		//set the count
		int rowCount = sqlite3_column_int(query, 0);
		*/
		std::string selectSQL = "SELECT * FROM user WHERE username = '" + std::string(username) + "';";
		std::cout << "\n" << selectSQL.c_str() << std::endl;
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		//sqlite3_step(query);
		else if (sqlite3_step(query) != SQLITE_ROW)
		{
			printf("Failed to select all from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}

		//if (rowCount > 0)
		//{
		std::cout << "Resulted username: " << sqlite3_column_text(query, 1) << std::endl;
		//Salt and hash the incoming password
		//get the user's salt from the query
		std::ostringstream saltstream;
		saltstream << sqlite3_column_text(query, 3);
		std::string salt = saltstream.str();
		std::string hashedIncomingPassword = HashPassword(password, salt); //hash the password that came from the user

		//Salt and hash the database password
		//get the password selected from the query
		std::ostringstream passwordstream;
		passwordstream << sqlite3_column_text(query, 2);
		std::string databasepassword = passwordstream.str();

		//commpare the 2 results
		//check to see if the client's password matches the one in the database
		if (hashedIncomingPassword == databasepassword)
		{
			//Password has been accepted. move on
			output = true;
			break;
		}
		else
		{
			//password has been rejected. do something else.
			output = false;
			break; //break out of the loop
		}
	}
	//else
	//{
		//username does not exist
		//output = false;
		//break;
	//}

	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();
	return output;
}
//shutdown database
//}

int LoginServer::FindNextID()
{
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string selectSQL = "SELECT count(*) AS COUNT FROM user;";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_ROW)
		{
			printf("Failed to count item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		//sqlite3_step(query);
		//else if (sqlite3_step(query) != SQLITE_DONE)
		//{
		//	printf("Failed to select item(s) from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
		//	break;
		//}
		break;
	}
	int output = sqlite3_column_int(query, 0);

	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();

	return output;
}

bool LoginServer::FindUsername(char * username)
{
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string selectSQL = " SELECT count(*) as COUNT, userID FROM user where username = '" + std::string(username) + "';";

		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_ROW)
		{
			printf("Failed to count usernames from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		//sqlite3_step(query);
	  //else if (sqlite3_step(query) != SQLITE_DONE)
	  //{
	  //	printf("Failed to select item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
	  //	break;
	  //}
		break;
	}
	//if the returned count is greater than 0, then the username was found
	if (sqlite3_column_int(query, 0) > 0)
	{
		sqlite3_finalize(query);
		sqlite3_close_v2(pDB);
		sqlite3_shutdown();
		return true;
	}
	//otherwise return false since the username was not found;
	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();
	return false;
}

bool LoginServer::EmptyDatabase()
{
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string selectSQL = " SELECT count(*) as COUNT, userID FROM user";

		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_ROW)
		{
			printf("Failed to count usernames from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		break;
	}
	bool output = false;
	//if the returned count is greater than 0, then there were items in the database
	if (sqlite3_column_int(query, 0) > 0)
	{
		output = true;	
	}
	else
	{
		output = false;
	}
	//otherwise return false since the username was not found;
	sqlite3_finalize(query);
	sqlite3_close_v2(pDB);
	sqlite3_shutdown();
	return output;
	
}

void LoginServer::handleNetworkMessages()
{
	std::cout << "Server started\n";
	RakNet::Packet* packet = nullptr;
	while (mbRunning)
	{
		for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
		{
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
				std::cout << "Connection incoming. \n";
				sendNewClientID(packet->systemAddress); //give my new client an ID
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				//client initiated a disconnection
				onClientDisconnection(packet);
				//m_pPeerInterface->CloseConnection(packet->systemAddress, false, 0, HIGH_PRIORITY);
				break;
			case ID_CONNECTION_LOST:
				//a packet couldnt be delivered so lose connection
				onClientTimeOut(packet->systemAddress);
				break;
			case ID_CLIENT_LOGIN:
				std::cout << "Login attempt incoming" << std::endl;
				onClientLogin(packet);
				break;
			case ID_CLIENT_REGISTER:
				std::cout << "Registration attempt incoming" << std::endl;
				onClientRegistration(packet);
				break;
			case ID_SERVER_TOKEN_REQUEST:
				onServerTokenRequest(packet);
				break;
			case ID_SERVER_CORRECT_TOKEN:
				onServerCorrectToken(packet);
				break;
			case ID_CLIENT_CLIENT_DATA:
				break;
			case ID_SERVER_CLIENT_LOGOUT_NOTIFICATION:
				onClientLogOut(packet);
				break;
			case ID_CLIENT_USERNAME_LOGOUT:
				std::cout << "The server didnt send me a notification" << std::endl;
			case ID_CLIENT_REMOTE_LOGOUT:
				RakNet::RakString username;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(username);
				for (auto it : mUserTokens)
				{
					if (strcmp(it.username, username) == 0)
					{
						strcpy_s(it.token, "");
					}
				}
				break;
			default:
				std::cout << "Unknown ID: " << packet->data[0] << std::endl;
				break;
			}
		}
	}
}

void LoginServer::onClientRegistration(RakNet::Packet * packet)
{
	User packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	//ignore bytes i dont want to read in
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData); //dump the packet data into the temp variable

	//do processing
	//First check for drop table shenanigans
	std::string username(packetData.username);
	std::string password(packetData.password);
	if (!username.find("DROP TABLE") > -1 && !password.find("DROP TABLE") > -1)
	{
		//if the database is empty, just inser the user
		if (!EmptyDatabase())
		{
			//otherwise look for the username
			if (!FindUsername((char*)username.c_str()))
			{
				Insert((char*)username.c_str(), (char*)password.c_str());
			}
			else
			{
				//tell the client username already exists
				RakNet::BitStream bs;
				bs.Write((RakNet::MessageID)ID_USERNAME_EXISTS);
				m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

			}
		}
		else
		{
			Insert((char*)username.c_str(), (char*)password.c_str());
		}
	}
	else
	{
		std::cout << "Found DROP TABLE No bamboozling here" << std::endl;
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_INVALID_USERPASS);
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}
}

void LoginServer::onClientLogin(RakNet::Packet * packet)
{
	std::cout << "Login attempt incoming " << std::endl;
	User packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);

	bool foundUser = false;
	for (auto it : mUserTokens)
	{
		if (strcmp(it.username, packetData.username) == 0)
		{
			foundUser = true;
			break;
		}
	}

	//login check buggy
	if (!foundUser)
	{
		if (AuthenticateUser((char*)packetData.username, (char*)packetData.password))
		{
			std::string Token = CreateSalt();
			std::cout << "Login attempt success" << std::endl;
			std::cout << "Login token: " << Token << std::endl;

			//store the token for the server request
			//UserTokens[packetData.username] = Token;

			LoginToken temp;
			strcpy_s(temp.username, packetData.username);
			strcpy_s(temp.token, Token.c_str());

			mUserTokens.push_back(temp);

			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_USERPASS_ACCEPTED);
			bs.Write(temp.token);
			std::cout << packet->systemAddress.ToString() << std::endl;
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);

		}
		else
		{
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_INVALID_USERPASS);
			bs.Write("Invalid username or password");
			m_pPeerInterface->Send(&bs, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			std::cout << "Login attempt failed" << std::endl;
		}
	}
	else
	{
		std::cout << "User already logged in" << std::endl;
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_LSERVER_ALREADY_LOGGED_IN);
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, packet->systemAddress, false);
	}
}

void LoginServer::onServerTokenRequest(RakNet::Packet * packet)
{
	std::cout << "The main server requested the token" << std::endl;
	RakNet::RakString TokenUsername;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(TokenUsername);

	//check to see if the username the server has requested exists
	
	for (auto it : mUserTokens)
	{
		//find the user
		if (strcmp(it.username, TokenUsername.C_String()) == 0)
		{
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_SERVER_TOKEN_FOUND);
			bs.Write(it);
			std::cout << "Token request sent " << std::endl;
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			break;
		}
	}
	
	LoginToken temp;
	for (auto it = mUserTokens.begin(); it != mUserTokens.end(); it++)
	{
		if (strcmp(it->username, TokenUsername.C_String()) == 0)
		{
			strcpy_s(temp.username, it->username);
			strcpy_s(temp.token, it->token);
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_SERVER_TOKEN_FOUND);
			bs.Write(temp);
			std::cout << "Token request sent " << std::endl;
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
			break;
		}
		else if (it == mUserTokens.end())
		{
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_SERVER_TOKEN_NOT_FOUND);
			std::cout << "Token request sent " << std::endl;
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
		}
	}

}

void LoginServer::onServerCorrectToken(RakNet::Packet * packet)
{
	std::cout << "LUL" << std::endl;
	RakNet::RakString username;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(username);

	for (auto it : mUserTokens)
	{
		if (strcmp(it.username, username) == 0)
		{
			strcpy_s(it.token, "");
		}
	}
}

void LoginServer::sendNewClientID(RakNet::SystemAddress address)
{
	//need to query the login server
	//get an empty slot
	RakNet::BitStream bs;
	//write into the packet with message ID of set client ID
	bs.Write((RakNet::MessageID)GameMessages::ID_MAIN_SET_ID);
	//set next client id as packet data
	int clientID = 0;
	//look for my array of potential clients and see if I have any remaining connections
	for (int i = 1; i < mClientAddresses.size(); ++i)
	{
		if (mClientAddresses[i] == RakNet::UNASSIGNED_SYSTEM_ADDRESS)
		{
			//found an empty slot
			clientID = i;
			//set this empty slot to the client's address
			mClientAddresses[i] = address;
			break;
		}
		else if (mClientAddresses[i] == address)
		{
			clientID = i++;
		}
	}
	bs.Write(clientID);
	//std::cout << address.ToString() << " Client ID: " << clientID << ", Just connected " << std::endl;
	std::cout << "sending a new client id" << std::endl;
	//send the new client an ID;
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, address, false);


}

void LoginServer::onClientLogOut(RakNet::Packet * packet)
{
	std::cout << "logging out a user from the list" << std::endl;
	RakNet::RakString username;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(username);

	int foundIndex = 0;
	for (int i = 0; i < mUserTokens.size(); ++i)
	{
		if (strcmp(mUserTokens[i].username, username.C_String()) == 0)
		{
			foundIndex = i;
			break;
		}
	}
	mUserTokens.erase(mUserTokens.begin() + foundIndex);
	//clear them so they can log in again.
}

std::string LoginServer::CreateSalt()
{
	
	CryptoPP::SecByteBlock key(CryptoPP::AES::DEFAULT_KEYLENGTH);
	std::string salt;

	CryptoPP::OS_GenerateRandomBlock(true, key, key.size());

	CryptoPP::HexEncoder hex(new CryptoPP::StringSink(salt));
	hex.Put(key, key.size());
	hex.MessageEnd();

	return salt;
	
}

std::string LoginServer::HashPassword(std::string password, std::string salt)
{
	
	std::string saltedPassword = password + salt;
	byte const* pbData = (byte*)saltedPassword.data();
	unsigned int dataLength = saltedPassword.length();
	byte abDigest[CryptoPP::SHA256::DIGESTSIZE];

	CryptoPP::SHA256().CalculateDigest(abDigest, pbData, dataLength);

	
	std::string output;
	CryptoPP::HexEncoder hex(new CryptoPP::StringSink(output));
	hex.Put(abDigest, sizeof(abDigest));
	hex.MessageEnd();

	
	return output;

}

std::string LoginServer::HashPassword(char * password, std::string salt)
{
	return HashPassword(std::string(password), salt);
}
