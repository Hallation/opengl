#pragma once
#include "Server.h"
#include "Structs.h"
#include "NetworkClient.h"

#include <unordered_map>
#include <vector>

class LoginServer :
	public Server
{
public:

	LoginServer();
	~LoginServer();

	void InitializeDatabase();
	//inserting into db with user and pw. salt generated within
	void Insert(char* username, char* password);
	bool AuthenticateUser(char* username, char* password);
	int FindNextID();
	bool FindUsername(char* username);
	bool EmptyDatabase();
	void handleNetworkMessages();

	void onClientRegistration(RakNet::Packet* packet);
	void onClientLogin(RakNet::Packet* packet);
	void onServerTokenRequest(RakNet::Packet* packet);
	void onServerCorrectToken(RakNet::Packet* packet);
	void sendNewClientID(RakNet::SystemAddress address);
	void onClientLogOut(RakNet::Packet* packet);
	std::string CreateSalt();
	std::string HashPassword(std::string password, std::string salt);
	std::string HashPassword(char* password, std::string salt);


private :
	std::unordered_map<char*, std::string> UserTokens;
	std::vector<LoginToken> mUserTokens;
	/*
	virtual void handleNetworkMessages();
	virtual void onClientDisconnection(RakNet::Packet* aPacket);
	virtual void onSetClientObject(RakNet::Packet* packet);
	virtual void onClientTimeOut(RakNet::SystemAddress& address);
	virtual void onReceiveClientData(RakNet::Packet* packet);
	//set client ID
	virtual void sendNewClientID(RakNet::SystemAddress& address);
	
	
	*/
};

