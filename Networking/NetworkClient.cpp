#include "NetworkClient.h"
#include "GameMessages.h"

#include "glm\gtc\quaternion.hpp"
#include "glm\gtx\quaternion.hpp"

#include <iostream>
#include <thread>
NetworkClient::NetworkClient()
{
	IP = "127.0.0.1";
	PORT = 7777;
	mbConnected = false;
	m_pPeerInterface = nullptr;

	//handleNetworkConnection();

}
//------------------------------------------------------------------------------------------------------------------------------------------
NetworkClient::~NetworkClient()
{
	//send server saying I disconnected

}
//------------------------------------------------------------------------------------------------------------------------------------------
glm::vec4 Interpolate(glm::vec4 aPos1, glm::vec4 aPos2, float t)
{
	return aPos1 * t + aPos2 * (1.f - t);
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::serverSetObject()
{
	//setup a bitstream 
	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)ID_CLIENT_SET_DATA);
	//write the gameobject to send to server
	bsOut.Write(miClientID);

	//send my data
	m_pPeerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::OnReceivedClientDataPacket(RakNet::Packet * packet)
{
	//receiving other packet data.
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

	int clientID;
	bsIn.Read(clientID);

}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onReceivedClientConnections(RakNet::Packet * packet)
{
	//ID_REMOTE_NEW_INCOMING_CONNECTION:
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

	int clientID;
	bsIn.Read(clientID);

	//load any new players
	if (clientID != miClientID)
	{
		std::cout << "Another client of ID: " << clientID << " Just connected" << std::endl;
		//add them to the dictionary

		//load a new incoming player
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onReceiveConnectedClients(RakNet::Packet * packet)
{
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	int clientID;
	bsIn.Read(clientID);
	std::cout << "Loading already connected players" << std::endl;

}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onRemoteDisconnection(RakNet::Packet * packet)
{
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	int clientID;
	bsIn.Read(clientID);
	std::cout << clientID << "Just disconnected" << std::endl;
	//find the disconnected client's object.
	//Then delete it.

}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onLostConnection(RakNet::Packet * packet)
{
	//does nothing
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onDisconnection()
{

}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::Run()
{
	//handle incoming packets
	if (mbConnected)
	{
		handleNetworkMessages();
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::SetConnectionLocation(char * aIPAddress, short aPORT)
{
	IP = aIPAddress;
	PORT = aPORT;
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::handleNetworkConnection()
{
	mbConnected = true;
	m_pPeerInterface = RakNet::RakPeerInterface::GetInstance();
	initalizeClientConnection();
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::initalizeClientConnection()
{
	//create a socket descriptor
	RakNet::SocketDescriptor sd;

	//startup my peer interface of 1 connection (only to a server);
	m_pPeerInterface->Startup(1, &sd, 1);

	std::cout << "Connecting to server: " << IP << ":" << PORT << std::endl;

	//try to connect to the server
	//peer interface -> connect to the IP @ port, with a set password, and the length of password in bytes
	RakNet::ConnectionAttemptResult res = m_pPeerInterface->Connect(IP, PORT, "password123", sizeof(char*));

	//check to see if we connected;
	if (res != RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		std::cout << "Unable to start connection, Error: " << res << std::endl;
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::Disconnect()
{
	if (mbConnected)
	{
		onDisconnection();
		//tell the client im disconnecting
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_DISCONNECTION_NOTIFICATION);
		bs.Write("hello");
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		//std::cout << "Client disconnecting" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(300));
		mbConnected = false;

		RakNet::SystemAddress temp(IP, PORT);
		m_pPeerInterface->CloseConnection(temp, true);

		delete m_pPeerInterface;
		m_pPeerInterface = nullptr;

		mbConnected = false;
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::handleNetworkMessages()
{
	while (true)
	{
		std::cout << "BEEP BOOP " << std::endl;
		//bare bones client
		RakNet::Packet* packet;
		for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
		{
			switch (packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				std::cout << "Another client has disconnected.\n";
				onRemoteDisconnection(packet);
				break;
			case ID_REMOTE_CONNECTION_LOST:
				std::cout << "Another client has lost the connection.\n";
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				//receiving a remote connection
				onReceivedClientConnections(packet);
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				std::cout << "Our connection request has been accepted.\n";
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				std::cout << "The server is full.\n";
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				std::cout << "We have been disconnected.\n";
				break;
			case ID_CONNECTION_LOST:
				std::cout << "Connection lost.\n";
				onLostConnection(packet);
				break;
			case ID_SERVER_TEXT_MESSAGE:
			{
				//receive the packet
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				//ignore the size of the message ID, //useless info, already dealth with
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

				//a Raknetstring to store our obtained mesage
				RakNet::RakString str;
				bsIn.Read(str); //read the data in our packet, and store it in str
				std::cout << str.C_String() << std::endl; //output the packet's message
				break;
			}
			case ID_SERVER_SET_CLIENT_ID:
				//handle incoming setting of client id
				onSetClientIDPacket(packet);
				//after client id is set, send my gameobject for server emulation
				serverSetObject();
				break;
			case ID_SERVER_TOKEN_REQUEST:
				std::cout << "received the return token" << std::endl;
				break;
			default:
				std::cout << "Received a message with a unknown id: " << packet->data[0] << std::endl;
				mbConnected = false;
				std::this_thread::sleep_for(std::chrono::milliseconds(50));
				break;
			}
		}
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::onSetClientIDPacket(RakNet::Packet * packet)
{
	//take the packet, store it, dont copy
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(miClientID); //throw the data into my clientID; who needs validation anyway.
	std::cout << "Client ID is : " << miClientID << std::endl;
}
//------------------------------------------------------------------------------------------------------------------------------------------
void NetworkClient::CreateConnection(char * ipAddress, int Port)
{
	IP = ipAddress;
	PORT = Port;
	handleNetworkConnection();
}
//------------------------------------------------------------------------------------------------------------------------------------------