#include "Server.h"

#include "GameMessages.h"

#include <MessageIdentifiers.h>
#include <RakPeerInterface.h>
#include <BitStream.h>

#include <DependentExtensions\SQLite3Plugin\SQLite3ServerPlugin.h>
#include <iostream>
#include <string>

#include <thread>
#include <chrono>
Server::Server()
{
	RCONPassword = "password123";
	miMaxClients = 12;
	PORT = 7777;
	m_pPeerInterface = nullptr;
	for (unsigned int i = 1; i <= miMaxClients; ++i)
	{
		//set everything to nothing
		mClientAddresses[i] = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
	}
	mbRunning = true;
}
//------------------------------------------------------------------------------------------------------------------------------------------
Server::~Server()
{
	if (m_pPeerInterface != nullptr)
	{
		delete m_pPeerInterface;
		m_pPeerInterface = nullptr;
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::Initialize()
{
	//startup the server
	std::cout << "Starting up the server..." << std::endl;

	//initialize peer interfac
	m_pPeerInterface = RakNet::RakPeerInterface::GetInstance();

	//describe my socket
	RakNet::SocketDescriptor sd(PORT, 0);

	//only allow 32 connections
	//startup max connections, socketdescriptor, descriptor count(size), thread priority
	m_pPeerInterface->Startup(miMaxClients, &sd, 1);
	m_pPeerInterface->SetMaximumIncomingConnections(miMaxClients);
	m_pPeerInterface->SetIncomingPassword("password123", sizeof(char*));

	//thread, method to put on thread, any parameters
	//make a connection to my login server
	//RakNet::ConnectionAttemptResult res = m_pPeerInterface->Connect("127.0.0.1", 9999, "password123", sizeof(char*));

}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::Run()
{
	handleNetworkMessages();
}

//------------------------------------------------------------------------------------------------------------------------------------------
void Server::handleNetworkMessages()
{
	std::cout << "Main Server started\n";
	RakNet::Packet* packet = nullptr;
	while (mbRunning)
	{
		for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
		{
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
				std::cout << "Connection incoming. \n";
				//sendNewClientID(packet->systemAddress); // dont send them an ID wait for server authentication.
				//send it any of the previous player's data
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				//client initiated a disconnection
				onClientDisconnection(packet);
				m_pPeerInterface->CloseConnection(packet->systemAddress, false, 0, HIGH_PRIORITY);
				break;
			case ID_CONNECTION_LOST:
				//a packet couldnt be delivered so lose connection
				onClientTimeOut(packet->systemAddress);
				break;
			case ID_CLIENT_CLIENT_DATA:
			{
				//receive new position data for whichever client is sending it
				onReceiveClientData(packet);
				break;
			}
			case ID_CLIENT_SET_DATA:
				std::cout << "Client set some new info" << std::endl;
				onSetClientObject(packet);
				break;
			case ID_SERVER_TOKEN_FOUND:
				std::cout << "Request returned from the login server " << std::endl;
				onTokenRequestReceived(packet);
				break;
			case ID_CLIENT_TOKEN:
				std::cout << "Client token received" << std::endl;
				onClientTokenReceived(packet);
				break;
			case ID_SERVER_TOKEN_NOT_FOUND:
				std::cout << "The token wasn't found on the login server" << std::endl;
				break;
			case ID_CLIENT_LOG_OUT:
				std::cout << "user is logging out" << std::endl;
				onClientLogOut(packet);
				break;
			default:
				std::cout << "Unknown ID: " << packet->data[0] << std::endl;
				break;
			}
		}
		//std::cout << "Beep " << std::endl;
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::sendClientPing()
{
	std::cout << "Ping thread started\n";
	while (true)
	{
		if (m_pPeerInterface->NumberOfConnections() > 0) {
			std::cout << "Ping sent" << std::endl;
			//bitstream char
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)GameMessages::ID_SERVER_TEXT_MESSAGE);
			bs.Write("Ping");
			//peerinterface->send bitstream, packet_priority, reliability, ordering channel
			//system identifier, broadcast, force receipt number
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0,
				RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			//sleep this thread for 1 second
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onClientDisconnection(RakNet::Packet* packet)
{
	//whenever the client sends a disconnection notification.
	int packetData;
	//once client disconnects
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);

	//look for the clients address.
	for (auto clients : mClientAddresses)
	{
		//if the clients system address was found
		if (clients.second == packet->systemAddress)
		{
			packetData = clients.first; //
		}
	}

	std::cout << mClientAddresses[packetData].ToString() << " of client ID: " << packetData << " Just disconnected " << std::endl;
	//Since I know which client this is, free up the connection slot
	mClientAddresses[packetData] = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
	//tell other clients someone has disconnected
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_REMOTE_DISCONNECTION_NOTIFICATION);
	bs.Write(packetData);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onSetClientObject(RakNet::Packet * packet)
{
	//have an array of client objects for emulation
	//setup a bitstream for reading
	RakNet::BitStream bs(packet->data, packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	//read in the clients gameobject
	int tempClientID;
	bs.Read(tempClientID);

	//read in the clients gameobject

}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onClientTimeOut(RakNet::SystemAddress & address)
{
	std::cout << address.ToString() << " Lost connection " << std::endl;
	//go through my client status array
	for (auto connection : mClientAddresses)
	{
		//once the address is found, set it to unassigned to free up the slot
		if (connection.second == address)
		{
			connection.second = RakNet::UNASSIGNED_SYSTEM_ADDRESS;
		}
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onReceiveClientData(RakNet::Packet * packet)
{
	RakNet::BitStream bs(packet->data, packet->length, false);
	//need to tell the other users this user's new location
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onNewClientConnection(RakNet::Packet * packet)
{
	
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::sendNewClientID(RakNet::SystemAddress & address)
{
	//need to query the login server
	//get an empty slot
	RakNet::BitStream bs;
	//write into the packet with message ID of set client ID
	bs.Write((RakNet::MessageID)GameMessages::ID_MAIN_SET_ID);
	//set next client id as packet data
	int clientID = 0;
	//look for my array of potential clients and see if I have any remaining connections
	for (int i = 1; i < mClientAddresses.size(); ++i)
	{
		if (mClientAddresses[i] == RakNet::UNASSIGNED_SYSTEM_ADDRESS)
		{
			//found an empty slot
			clientID = i;
			//set this empty slot to the client's address
			mClientAddresses[i] = address;
			break;
		}
		else if (mClientAddresses[i] == address)
		{
			clientID = i++;
		}
	}
	bs.Write(clientID);
	//std::cout << address.ToString() << " Client ID: " << clientID << ", Just connected " << std::endl;
	std::cout << "sending a new client id" << std::endl;
	//send the new client an ID;
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, address, false);

	
	//tell other peers there is a new client
	RakNet::BitStream newPlayer;
	newPlayer.Write((RakNet::MessageID)ID_REMOTE_NEW_INCOMING_CONNECTION);
	newPlayer.Write(clientID);
	m_pPeerInterface->Send(&newPlayer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	//send other clients any currently connected clients
	for (auto client : mClientAddresses)
	{
		if (client.second != address && client.second != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
		{
			RakNet::BitStream otherPlayer;
			otherPlayer.Write((RakNet::MessageID)ID_REMOTE_CONNECTED_CLIENTS);
			otherPlayer.Write(client.first);
			m_pPeerInterface->Send(&otherPlayer, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onClientLogOut(RakNet::Packet * packet)
{
	RakNet::RakString packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);

	std::cout << packetData.C_String() << " Is logging out " << std::endl;
	int index = -1;
	//find the user
	for (int i = 0; i < mClientTokens.size(); ++i)
	{
		if (strcmp(mClientTokens[i].username, packetData.C_String()) == 0)
		{
			//when found break out of loop
			index = i;
			break;
		}
	}

	if (index != -1) 
	{
		mClientTokens.erase(mClientTokens.begin() + index);
		mClientInfo.erase(mClientInfo.begin() + index);
	}
	//need to tell the login server I have logged out
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_SERVER_CLIENT_LOGOUT_NOTIFICATION);
	bs.Write(packetData.C_String());
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, loginServerAddress, false);
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::Destroy()
{
	delete m_pPeerInterface;
	m_pPeerInterface = nullptr;
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onTokenRequestReceived(RakNet::Packet * packet)
{
	std::cout << "The token was found on login server " << std::endl;
	LoginToken packetData; //the server's login token
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);
	bool invalidtoken = false;
	for (auto it : mClientTokens)
	{
		std::cout << "Beep boop" << std::endl;
		//look for the user that came in
		std::cout << it.username << std::endl;
		std::cout << packetData.username << std::endl;
		if (strcmp(it.username, packetData.username) == 0)
		{
			std::cout << "Comparing the 2 tokens";
			//need to query the login for token
			std::cout << strcmp(it.token, packetData.token) << std::endl;
			if (strcmp(it.token, packetData.token) == 0)
			{
				//need to send to the right user
				//find the system address that should** be binded to the client's username
				for (auto jt : mClientInfo)
				{
					if (strcmp(it.username, jt.username)== 0)
					{
						std::cout << "Token was correct, sending the client their ID" << std::endl; 
						//copy nothing into the token so it can't be reused. need to also tell the login server to clear it.
						strcpy_s(it.token, "");
						onCorrectToken(packetData.username);
						sendNewClientID(jt.clientAddress);
					}
				}
				
			}
			else
			{
				invalidtoken = true;
				//DESTROY THEM 
				RakNet::BitStream bs;
				bs.Write((RakNet::MessageID)ID_INVALID_TOKEN);
				for (auto jt : mClientInfo)
				{
					if (strcmp(it.username, jt.username) == 0)
					{
						m_pPeerInterface->Send(&bs, MEDIUM_PRIORITY, RELIABLE, 0, jt.clientAddress, false); //tell the user the token was invalid. need to also clear the user
					}
				}
			}
		}
	} //end of loop

	if (invalidtoken)
	{ 
		int clientIndex = 0;
		for (int i = 0; i < mClientTokens.size(); ++i)
		{
			if (strcmp(mClientTokens.at(i).username, packetData.username))
			{
				clientIndex = i;
				break;
			}
		}
		//remove them from the vector so no further processing can be done with them.
		mClientTokens.erase(mClientTokens.begin() + clientIndex);
		mClientInfo.erase(mClientInfo.begin() + clientIndex);
		invalidtoken = false;
	}


}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onClientTokenReceived(RakNet::Packet * packet)
{
	//read in the logged in users login token

	LoginToken packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);

	mClientTokens.push_back(packetData); //store the user's username and token;
	//we need to store the user's system address. could use a memory only database. oh well no time
	ClientInfo temp;
	strcpy_s(temp.username, packetData.username);
	temp.clientAddress = packet->systemAddress;
	mClientInfo.push_back(temp);
	//std::cout << strcmp(clientToken, LoginToken.c_str()) << std::endl;

	//request the token from the server
	std::cout << "Sending a token request to login server" << std::endl;
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_SERVER_TOKEN_REQUEST);
	bs.Write(packetData.username);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, loginServerAddress, false);

}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::SetLoginServerIP(char * aIPAddress, short aPORT)
{
	loginServerAddress = RakNet::SystemAddress(aIPAddress, aPORT);
	RakNet::ConnectionAttemptResult res = m_pPeerInterface->Connect(aIPAddress, aPORT, "password123", sizeof(char*));
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::RunLoginServerConnection()
{
	std::cout << "Running login server" << std::endl;

}
//------------------------------------------------------------------------------------------------------------------------------------------
void Server::onCorrectToken(char* aUser)
{
	//send the login server which token to clear
	std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
	RakNet::BitStream bs;
	bs.Write((RakNet::MessageID)ID_SERVER_CORRECT_TOKEN);
	bs.Write(aUser);
	m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, loginServerAddress, false);
}
//------------------------------------------------------------------------------------------------------------------------------------------