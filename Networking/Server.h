#pragma once

#include "ServerClient.h"
#include "Structs.h"

#include <MessageIdentifiers.h>
#include <RakPeerInterface.h>
#include <BitStream.h>

#include <thread>
#include <chrono>
#include <unordered_map>
#include <vector>
class Server
{
public:
	Server();
	~Server();

	void Initialize();
	void Run();

	//send and receive data
	void sendClientPing();
	virtual void handleNetworkMessages();
	virtual void onClientDisconnection(RakNet::Packet* aPacket);
	virtual void onSetClientObject(RakNet::Packet* packet);
	virtual void onClientTimeOut(RakNet::SystemAddress& address);
	virtual void onReceiveClientData(RakNet::Packet* packet);
	virtual void onNewClientConnection(RakNet::Packet* packet);
	//set client ID
	virtual void sendNewClientID(RakNet::SystemAddress& address);
	virtual void onClientLogOut(RakNet::Packet* packet);
	virtual void Destroy();
	void onTokenRequestReceived(RakNet::Packet* packet);
	void onClientTokenReceived(RakNet::Packet* packet);
	void SetLoginServerIP(char* aIPAddress, short aPORT);
	void onCorrectToken(char* aUser);
	void RunLoginServerConnection();

	bool mbRunning;
protected:
	RakNet::SystemAddress loginServerAddress;

	RakNet::RakPeerInterface* m_pPeerInterface;

	std::unordered_map<int, RakNet::SystemAddress > mClientAddresses;
	
	std::vector<LoginToken> mClientTokens;
	std::vector<ClientInfo> mClientInfo;
	unsigned int miMaxClients;
	unsigned short PORT;

	std::string RCONPassword;

};

