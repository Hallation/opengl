#include "ServerClient.h"

#include "GameMessages.h"
#include <iostream>
#include <thread>
#include <chrono>


ServerClient::ServerClient()
{
}


ServerClient::~ServerClient()
{
}

void ServerClient::SetServer(char * aIP, int aPORT)
{
	IP = aIP;
	PORT = aPORT;
}

void ServerClient::handleNetworkMessages()
{

	while (true)
	{

		//bare bones client
		RakNet::Packet* packet;
		for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
		{
			switch (packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				std::cout << "Another client has disconnected.\n";
				onRemoteDisconnection(packet);
				break;
			case ID_REMOTE_CONNECTION_LOST:
				std::cout << "Another client has lost the connection.\n";
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				//receiving a remote connection
				onReceivedClientConnections(packet);
				break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
				std::cout << "Our connection request has been accepted.\n";
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				std::cout << "The server is full.\n";
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				std::cout << "We have been disconnected.\n";
				break;
			case ID_CONNECTION_LOST:
				std::cout << "Connection lost.\n";
				onLostConnection(packet);
				break;
			case ID_SERVER_TEXT_MESSAGE:
			{
				//receive the packet
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				//ignore the size of the message ID, //useless info, already dealth with
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

				//a Raknetstring to store our obtained mesage
				RakNet::RakString str;
				bsIn.Read(str); //read the data in our packet, and store it in str
				std::cout << str.C_String() << std::endl; //output the packet's message
				break;
			}
			case ID_SERVER_SET_CLIENT_ID:
				//handle incoming setting of client id
				onSetClientIDPacket(packet);
				//after client id is set, send my gameobject for server emulation
				serverSetObject();
				break;
			case ID_SERVER_TOKEN_REQUEST:
				std::cout << "received the return token" << std::endl;
				break;
			default:
				std::cout << "Received a message with a unknown id: " << packet->data[0] << std::endl;
				mbConnected = false;
				std::this_thread::sleep_for(std::chrono::milliseconds(50));
				break;
			}
		}
	}
}

void ServerClient::Run()
{
	if (mbConnected) 
	{
		handleNetworkMessages();
	}
}
