#pragma once
#include "glm\ext.hpp"
#include <RakPeerInterface.h>

struct GameObject
{
	glm::vec4 mvPosition;
	glm::vec4 mvColour;
};

struct User
{
	char username[32];
	char password[32];
};


struct LoginToken
{
	char username[32];
	char token[64];
};

struct ClientInfo
{
	char username[32];
	RakNet::SystemAddress clientAddress;
};