#include "AABoundingBox.h"

// Default constructor.
AABoundingBox::AABoundingBox()
{
	Reset();
}

// Default destructor.
AABoundingBox::~AABoundingBox()
{

}

// Resets member variables that are used.
void AABoundingBox::Reset()
{
	m_v3Minimum.x = m_v3Minimum.y = m_v3Minimum.z = 1e37f;
	m_v3Maximum.x = m_v3Maximum.y = m_v3Maximum.z = -1e37f;
}

void AABoundingBox::Fit(const std::vector<glm::vec3>& a_vPoints)
{
	for (auto& aP : a_vPoints)
	{
		if (aP.x < m_v3Minimum.x)
		{
			m_v3Minimum.x = aP.x;
		}
		if (aP.y < m_v3Minimum.y)
		{
			m_v3Minimum.y = aP.y;
		}
		if (aP.z < m_v3Minimum.z)
		{
			m_v3Minimum.z = aP.z;
		}
		if (aP.x > m_v3Maximum.x)
		{
			m_v3Maximum.x = aP.x;
		}
		if (aP.y > m_v3Maximum.y)
		{
			m_v3Maximum.y = aP.y;
		}
		if (aP.z > m_v3Maximum.z)
		{
			m_v3Maximum.z = aP.z;
		}
	}
}