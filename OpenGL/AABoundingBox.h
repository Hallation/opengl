#pragma once

#include "BaseBounding.h"

class AABoundingBox : public BaseBounding
{
public:

	// Default constructor.
	AABoundingBox();
	// Default destructor.
	~AABoundingBox();

	// Resets member variables that are used.
	void Reset();

	void Fit(const std::vector<glm::vec3>& a_vPoints);

private:

protected:

};