#include "AnimatedModel3D.h"
#include "gl_core_4_4.h"
#include "stb\stb_image.h"
#include <iostream>
#include "IncludeHelpers.h"
AnimatedModel3D::AnimatedModel3D()
{
}

AnimatedModel3D::AnimatedModel3D(const char * objLocation, const char * objName, const char* objName2)
{
	std::string err;
	tinyobj::attrib_t attribs[2];
	std::vector<tinyobj::shape_t> shapes[2];
	std::vector<tinyobj::material_t> materials[2];

	std::string obj(objLocation);
	obj += objName;
	tinyobj::LoadObj(&attribs[0], &shapes[0], &materials[0], &err, obj.c_str(), objLocation);
	obj = "";
	obj += objLocation;
	obj += objName2;
	tinyobj::LoadObj(&attribs[1], &shapes[1], &materials[1], &err, obj.c_str(), objLocation);

	for (int i = 0; i < (int)shapes[0].size(); ++i)
	{
		OpenGLInfo gl;

		gl.m_VBO = createVertexBuffer(attribs[0], shapes[0][i]);
		gl.m_VBO1 = createVertexBuffer(attribs[1], shapes[1][i]);

		gl.m_faceCount = shapes[0][i].mesh.num_face_vertices.size();

		glGenVertexArrays(1, &gl.m_VAO);
		glBindVertexArray(gl.m_VAO);

		//bind first vbo
		glBindBuffer(GL_ARRAY_BUFFER, gl.m_VBO);
		glEnableVertexAttribArray(0);// vert position
		glEnableVertexAttribArray(1);//norms
		glEnableVertexAttribArray(2);//uvs

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)16);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)32);

		//bind first vbo
		glBindBuffer(GL_ARRAY_BUFFER, gl.m_VBO1);
		glEnableVertexAttribArray(3);//second vert position
		glEnableVertexAttribArray(4);//second set of norms

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0); //second positions
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)16); //second norms

		glBindVertexArray(0);
		mOpenGlInfo.push_back(gl);
	}

}


AnimatedModel3D::~AnimatedModel3D()
{

}


unsigned int AnimatedModel3D::createVertexBuffer(const tinyobj::attrib_t & a_Attribs, const tinyobj::shape_t & shape)
{
	float scale = 10.f;
	std::vector<OBJVertex> verts;
	int shapeIndex = 0;
	for (auto face : shape.mesh.num_face_vertices)
	{
		for (int i = 0; i < 3; ++i)
		{
			tinyobj::index_t idx = shape.mesh.indices[shapeIndex + i];

			OBJVertex v = { 0 };

			//vert positions
			v.x = a_Attribs.vertices[3 * idx.vertex_index + 0] * scale + mGameObject.mvPosition.x;;
			v.y = a_Attribs.vertices[3 * idx.vertex_index + 1] * scale + mGameObject.mvPosition.y;;
			v.z = a_Attribs.vertices[3 * idx.vertex_index + 2] * scale + mGameObject.mvPosition.z;;
			//normals
			if (a_Attribs.normals.size() > 0)
			{
				v.nx = a_Attribs.normals[3 * idx.normal_index + 0];
				v.ny = a_Attribs.normals[3 * idx.normal_index + 1];
				v.nz = a_Attribs.normals[3 * idx.normal_index + 2];
			}
			//texture coords
			if (a_Attribs.texcoords.size() > 0)
			{
				v.u = a_Attribs.texcoords[2 * idx.texcoord_index + 0];
				v.v = 1 - a_Attribs.texcoords[2 * idx.texcoord_index + 1];
			}
			verts.push_back(v);
		}
		shapeIndex += face;
	}

	//bind the vert data
	unsigned int buffer = 0;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(OBJVertex), verts.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return buffer;
}

void AnimatedModel3D::Initialize(const char * objLocation, const char * objName, const char * objName2)
{
	std::string err;
	tinyobj::attrib_t attribs[2];
	std::vector<tinyobj::shape_t> shapes[2];
	std::vector<tinyobj::material_t> materials[2];

	std::string obj(objLocation);
	obj += objName;
	tinyobj::LoadObj(&attribs[0], &shapes[0], &materials[0], &err, obj.c_str(), objLocation);
	obj = "";
	obj += objLocation;
	obj += objName2;
	tinyobj::LoadObj(&attribs[1], &shapes[1], &materials[1], &err, obj.c_str(), objLocation);

	for (int i = 0; i < (int)shapes[0].size(); ++i)
	{
		OpenGLInfo gl;

		gl.m_VBO = createVertexBuffer(attribs[0], shapes[0][i]);
		gl.m_VBO1 = createVertexBuffer(attribs[1], shapes[1][i]);

		gl.m_faceCount = shapes[0][i].mesh.num_face_vertices.size();

		glGenVertexArrays(1, &gl.m_VAO);
		glBindVertexArray(gl.m_VAO);

		//bind first vbo
		glBindBuffer(GL_ARRAY_BUFFER, gl.m_VBO);
		glEnableVertexAttribArray(0);// vert position
		glEnableVertexAttribArray(1);//norms
		glEnableVertexAttribArray(2);//uvs

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)16);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)32);

		//bind first vbo
		glBindBuffer(GL_ARRAY_BUFFER, gl.m_VBO1);
		glEnableVertexAttribArray(3);//second vert position
		glEnableVertexAttribArray(4);//second set of norms

		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0); //second positions
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)16); //second norms

		glBindVertexArray(0);
		mOpenGlInfo.push_back(gl);
	}

}

void AnimatedModel3D::Draw(glm::mat4 ProjectionView, glm::mat4 CameraWorld, glm::vec4 lightColour)
{
	if (mCulling.SphereCullCheck(ProjectionView, mBoundingSphere.GetCentre(), mBoundingSphere.GetRadius())) {
		for (auto& gl : mOpenGlInfo)
		{
			m_texture.DrawTexture();
			int loc = glGetUniformLocation(m_texture.GetProgramID(), "projectionView");
			glUniformMatrix4fv(loc, 1, GL_FALSE, &ProjectionView[0][0]);
			loc = glGetUniformLocation(m_texture.GetProgramID(), "keyTime");
			glUniform1f(loc, cosf((float)glfwGetTime()));

			glBindVertexArray(gl.m_VAO);
			glDrawArrays(GL_TRIANGLES, 0, gl.m_faceCount * 3);
		}
	}
}

void AnimatedModel3D::DrawVerts(glm::mat4 ProjectionView)
{
	if (mCulling.SphereCullCheck(ProjectionView, mBoundingSphere.GetCentre(), mBoundingSphere.GetRadius()))
	{
		for (auto& gl : mOpenGlInfo)
		{
			glBindVertexArray(gl.m_VAO);
			glDrawArrays(GL_TRIANGLES, 0, gl.m_faceCount * 3);
		}
	}

}

void AnimatedModel3D::Animate()
{
	for (auto& gl : mOpenGlInfo)
	{
		glBindVertexArray(gl.m_VAO);
		glDrawArrays(GL_TRIANGLES, 0, gl.m_faceCount * 3);
	}
}

void AnimatedModel3D::Update(float deltatime)
{

}

void AnimatedModel3D::LoadShader(const char * vertShader, const char * fragShader)
{
	m_texture.LoadShaders(vertShader, fragShader);
}



