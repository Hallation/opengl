#pragma once
#include "Model3D.h"

class AnimatedModel3D :
	public Model3D
{
public:
	AnimatedModel3D();
	AnimatedModel3D(const char* objLocation, const char* objName, const char* objName2);
	~AnimatedModel3D();

	unsigned int createVertexBuffer(const tinyobj::attrib_t& attribs, const tinyobj::shape_t& shape);
	void Initialize(const char* objLocation, const char* objName, const char* objName2);
	//void Draw();
	void Draw(glm::mat4 ProjectionView, glm::mat4 CameraWorld, glm::vec4 lightColour);
	void DrawVerts(glm::mat4 ProjectionView);
	void Animate();
	void Update(float deltatime);
	
	void LoadShader(const char* vertShader, const char* fragShader);
	/*
	protected:
	glm::vec4 mPosition;
	std::string msErr;
	tinyobj::attrib_t m_Attribs;
	std::vector<tinyobj::shape_t> mShapes;
	std::vector<tinyobj::material_t> mMaterials;
	std::vector<OpenGLInfo> mOpenGlInfo;
	*/
	//mOpenGlInfo = m_meshes
};

