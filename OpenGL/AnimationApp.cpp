#include "IncludeHelpers.h"
#include "AnimationApp.h"
#include "FPSCamera.h"
#include "AnimatedModel3D.h"
#include "Grid.h"
#include "Plane3D.h"
#include "ParticleEmitter.h"

#include "GLFW\glfw3.h"
#include "stb\stb_image.h"
#include "ImGUIImplement.h"

AnimationApp::AnimationApp()
{
}


AnimationApp::~AnimationApp()
{
}

bool AnimationApp::Startup()
{
	mbGuiActive = false;
	lightColour = glm::vec4(1);
	lightDir = glm::vec3(1);
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);

	//initilizae glfw
	Application::Initialize();
	Application::InitializeShadows();
	//programs used for testing only

	//declare my hand
	mhand = new AnimatedModel3D();
	mhand->SetPosition(glm::vec4(10, 0, 20, 1));
	mhand->SetSphereOffset(glm::vec3(0, 4, 0));
	mhand->SetSphereRadius(2);
	mhand->Initialize("../bin/data/models/hand/", "hand_00.obj", "hand_37.obj");
	mhand->LoadTexture("../bin/data/models/hand/hand.png");
	mhand->LoadShader("../bin/data/shaders/vertShaders/AnimationVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");

	//declare my soulspear
	SoulSpearOBJ = new Model3D();
	SoulSpearOBJ->SetPosition(glm::vec4(6, 0.5f, 6, 1));
	SoulSpearOBJ->SetSphereOffset(glm::vec3(0, 2, 0));
	SoulSpearOBJ->SetSphereRadius(2);
	SoulSpearOBJ->Initialize("../bin/data/models/soulspear/", "soulspear.obj");
	SoulSpearOBJ->LoadShaders("../bin/data/shaders/vertShaders/lightingVS.glsl", "../bin/data/shaders/fragShaders/lightingFS.glsl");
	SoulSpearOBJ->LoadTexture("../bin/data/models/soulspear/soulspear_diffuse.tga", "../bin/data/models/soulspear_normal.tga");
	//"../bin/data/models/soulspear/soulspear_diffuse.tga"

	//make a "grid" really a plane
	mGrid = new Grid();
	mGrid->SetPosition(glm::vec4(-15.f, 0.f, -10.f,1));
	mGrid->Generate(64, 64);
	mGrid->LoadShaders("../bin/data/shaders/vertShaders/basicVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");

	mPlane = new Plane3D(nullptr);
	//load the bunnyobj
	BunnyOBJ = new Model3D();
	BunnyOBJ->SetPosition(glm::vec4(0, 0, 0,1));
	BunnyOBJ->SetSphereOffset(glm::vec3(0, 2, 0));
	BunnyOBJ->SetSphereRadius(4);
	BunnyOBJ->Initialize("../bin/data/models/stanford/", "Bunny.obj");

	m_texture = TextureLoader::LoadTexture("../bin/data/textures/four/four_diffuse.tga");
	//max particles, emit rate, min lifetime, max lifetime, min velocity, max velocitym start size, end size, start colour, end colour
	mEmitter = new ParticleEmitter();
	mEmitter->SetPosition(15, 0, 20);
	mEmitter->Initialise(200, 500, //max particles, emit rate
		0.1f, 1.0f, //min lifetime, max lifetime
		1, 5, //min velocity, max velocity
		1, 0.1f, //start size, end size
		glm::vec4(1, 0, 0, 1), glm::vec4(1, 1, 0, 1)); //start colour, end colour
	mEmitter->InitialiseShader();
	//turn on depth test
	glEnable(GL_DEPTH_TEST);
	//create my GUI
	ImGui_ImplGlfwGL3_Init(mWindow, true);
	Gizmos::create(1000, 1000, 1000, 1000);
	return true;
}

bool AnimationApp::Shutdown()
{
	//free memory
	delete SoulSpearOBJ;
	delete mGrid;
	delete mCamera;
	delete mhand;
	delete mEmitter;
	delete BunnyOBJ;
	delete mPlane;
	Gizmos::destroy();
	ImGui_ImplGlfwGL3_Shutdown();
	glfwDestroyWindow(mWindow);
	glfwTerminate();
	return true;
}

void AnimationApp::Draw()
{
	Gizmos::clear();
	ImGui_ImplGlfwGL3_NewFrame();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); 
	//float specPow = 128;

	glClearColor(clearColour.x, clearColour.y, clearColour.z, 1);

	//generate shadows
	GenerateShadows();
	mGrid->DrawVerts(mCamera->GetProjectionView());
	BunnyOBJ->DrawVerts(mCamera->GetProjectionView());
	SoulSpearOBJ->DrawVerts(mCamera->GetProjectionView());
	
	//draw shadows
	DrawShadows();
	mGrid->DrawVerts(mCamera->GetProjectionView());
	BunnyOBJ->DrawVerts(mCamera->GetProjectionView());
	
	
	//draw that grid
	mGrid->Draw(mCamera->GetProjectionView());

	//draw my particle emitter
	mEmitter->BindCamera(mCamera->GetProjectionView());
	mEmitter->Draw();
	//draw my soulspear obj, light colour of 1,1,1,1
	glm::vec3 light(sin(glfwGetTime()), 1, cos(glfwGetTime()));
	SoulSpearOBJ->Draw(mCamera->GetProjectionView(), -mCamera->GetWorldTransform(), lightColour, -glm::vec4(lightDir,1));
	//draw my hand 
	mhand->Draw(mCamera->GetProjectionView(), mCamera->GetWorldTransform(), glm::vec4(1));

	//draw gui over everything else
	ImGui::Begin("Instructions");
	ImGui::SetWindowPos(ImVec2(1000, 100));
	ImGui::SetWindowSize(ImVec2(200, 100));
	ImGui::Text("Open Options: P");
	ImGui::Text("Close Options: O");
	ImGui::End();
	if (mbGuiActive)
	{
		ImGui::Begin("Options:");
		ImGui::SetWindowPos(ImVec2(100, 100));
		ImGui::SetWindowSize(ImVec2(500, 150));
		ImGui::ColorEdit3("Clear colour", glm::value_ptr(clearColour));
		ImGui::ColorEdit3("Soulspear Light Colour", glm::value_ptr(lightColour));
		float lightDirection[] = { lightDir.x, lightDir.y, lightDir.z };
		ImGui::SliderFloat3("Soulspear Light Direction", lightDirection,0.0f, 1.0f);
		lightDir = glm::vec3(lightDirection[0], lightDirection[1], lightDirection[2]);

		ImGui::End();
	}
	ImGui::Render();
	Gizmos::draw(mCamera->GetProjectionView());
	
}

void AnimationApp::Run()
{
	Startup();
	deltaTime = 0;
	prevTime = 0;
	glfwMakeContextCurrent(mWindow);
	//glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	while (glfwWindowShouldClose(mWindow) == false && glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		currTime = (float)glfwGetTime();
		deltaTime = currTime - prevTime;
		prevTime = currTime; //deltatime

		//updates every frame
		Update(deltaTime);
		//draws every frame
		Draw();

		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}
}

void AnimationApp::Update(float deltatime)
{
	if (!mbGuiActive)
	{
		mCamera->Update(mWindow, deltatime);
	}

	if (glfwGetKey(mWindow, GLFW_KEY_P) == GLFW_PRESS)
	{
		mbGuiActive = true;
	}
	if (glfwGetKey(mWindow, GLFW_KEY_O) == GLFW_PRESS)
	{
		mbGuiActive = false;
	}

	mEmitter->Update(deltatime, -mCamera->GetProjectionView());
}
