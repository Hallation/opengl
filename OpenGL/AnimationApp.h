#pragma once
#include "Application.h"

class Grid;
class Model3D;
class Plane3D;
class AnimatedModel3D;
class ParticleEmitter;

class AnimationApp :
	public Application
{
public:
	AnimationApp();
	~AnimationApp();

	bool Startup();
	bool Shutdown();
	
	void Update(float deltatime);
	void Draw();
	void Run();

private:
	ParticleEmitter* mEmitter;
	AnimatedModel3D* mhand;
	Model3D* SoulSpearOBJ;
	Model3D* BunnyOBJ;
	Grid* mGrid;
	Plane3D* mPlane;
	
	unsigned int m_texture;
	unsigned int m_texture2;
	unsigned int programID;

	glm::vec3 lightDir;
	glm::vec4 lightColour;
};

