﻿#include "FPSCamera.h"
#include "Application.h"
#include "gl_core_4_4.h"
#include "AIE\Gizmos.h"
#include "Model3D.h"
#include "IncludeHelpers.h"
#include "ParticleEmitter.h"
#include "Plane3D.h"
#include <GLFW/glfw3.h>
#include <iostream>
/*

▒▒▒▒▒▒▄▄██████▄
▒▒▒▒▒▒▒▒▒▒▄▄████████████▄
▒▒▒▒▒▒▄▄██████████████████
▒▒▒▄████▀▀▀██▀██▌███▀▀▀████
▒▒▐▀████▌▀██▌▀▐█▌████▌█████▌
▒▒█▒▒▀██▀▀▐█▐█▌█▌▀▀██▌██████
▒▒█▒▒▒▒████████████████████▌
▒▒▒▌▒▒▒▒█████░░░░░░░██████▀
▒▒▒▀▄▓▓▓▒███░░░░░░█████▀▀
▒▒▒▒▀░▓▓▒▐█████████▀▀▒
▒▒▒▒▒░░▒▒▐█████▀▀▒▒▒▒▒▒
▒▒░░░░░▀▀▀▀▀▀▒▒▒▒▒▒▒▒▒
▒▒▒░░░░░░░░▒▒
*/
Application::Application()
{

	//stbi_load(filename, x, y, comp, req_comp);
}

Application::~Application()
{
}

void Application::InitializeShadows()
{
	//generate a shadow map
	//gen shadow map buffer
	glGenFramebuffers(1, &mFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	
	glGenTextures(1, &mFBODepth);
	glBindTexture(GL_TEXTURE_2D, mFBODepth);
	
	//a 16bit depth component
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//attach as a depth attachment to capture depth
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mFBODepth, 0);
	//no colour targets
	glDrawBuffer(GL_NONE);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer error!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//shadows generate [1]
	mGenerateShadow = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/GenerateShadowVS.glsl", "../bin/data/shaders/fragShaders/GenerateShadowFS.glsl");
	//use shadows [2]
	mUseShadow = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/UseShadowVS.glsl", "../bin/data/shaders/fragShaders/UseShadowFS.glsl");

	mLightDirection = glm::normalize(glm::vec3(1, 2.5f, 1));
	mLightProjection = glm::ortho<float>(-10, 10, -10, 10, -10, 10);
	mLightView = glm::lookAt(mLightDirection, glm::vec3(0), glm::vec3(0, 1, 0));
	mLightMatrix = mLightProjection * mLightView;
}

void Application::GenerateShadows()
{
	//generate the shadows
	glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	glViewport(0, 0, 1024, 1024);
	glClear(GL_DEPTH_BUFFER_BIT);

	glUseProgram(mGenerateShadow);
	int loc = glGetUniformLocation(mGenerateShadow, "lightMatrix");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &(mLightMatrix[0][0]));
}

void Application::DrawShadows()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 1280, 720);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//bind use shadow program
	glUseProgram(mUseShadow);
	//bind camera
	int loc = glGetUniformLocation(mUseShadow, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &(mCamera->GetProjectionView()[0][0]));
	//bind light matrix;
	glm::mat4 textureSpaceOffset(
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f
	);
	glm::mat4 lightMatrix = textureSpaceOffset * mLightMatrix;

	loc = glGetUniformLocation(mUseShadow, "lightMatrix");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &lightMatrix[0][0]);

	loc = glGetUniformLocation(mUseShadow, "lightDir");
	glUniform3fv(loc, 1, &mLightDirection[0]);

	loc = glGetUniformLocation(mUseShadow, "shadowMap");
	glUniform1i(loc, 0);

	loc = glGetUniformLocation(mUseShadow, "shadowBias");
	glUniform1f(loc, 0.1f);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mFBODepth);
}

//for gizmos 
bool Application::Initialize()
{
	currTime = 0;
	prevTime = 0;
	deltaTime = 0;
	//initlizae glfw
	if (glfwInit() == false)
		return false;

	//define the window
	GLFWwindow* window = glfwCreateWindow(1280, 720, "graphics", nullptr, nullptr);
	mWindow = window; //set mwindow pointer

	if (window == nullptr)
	{
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(mWindow);

	//load gl function
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(mWindow);
		glfwTerminate();
		return false;
	}
	auto major = ogl_GetMajorVersion();
	auto minor = ogl_GetMinorVersion();
	printf("GL: %i.%i\n", major, minor);
	glfwGetWindowSize(mWindow, &iWidth, &iHeight);
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glEnable(GL_DEPTH_TEST);
	return true;
}
void Application::MakeDefaultCam()
{
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);
}
bool Application::Startup()
{
#pragma region startup
	//define my camera
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);

	if (!Initialize())
	{
		std::cout << "something broke in initilizaiton";
		return false;
	}

	InitializeShadows();
#pragma region Shaders
	//basic lighting [0]
	mBasicProgram = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/lightingVS.glsl", "../bin/data/shaders/fragShaders/lightingFS.glsl");
	glClearColor(0.25f, 0.25f, 0.25f, 1); //clear color
	glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	//make a place from 10 wide plane
	mGrid = new Grid();
	mGrid->SetPosition(glm::vec4(-10, 0, -10,1));
	mGrid->Generate(50, 50);

	Vertex vertexData[] =
	{
		{ -10,0, 10,1,0,1,0,0,1,0,0,0,0,1 },
		{ 10,0, 10,1,0,1,0,0,1,0,0,0,1,1 },
		{ 10,0,-10,1,0,1,0,0,1,0,0,0,1,0 },
		{ -10,0,-10,1,0,1,0,0,1,0,0,0,0,0 },
	};
	//index data
	unsigned int indexData[] =
	{
		0,1,2,
		0,2,3,
	};
	//load models
	BunnyOBJ = new Model3D();
	//BunnyOBJ = new Model3D("../bin/data/models/stanford/", "Bunny.obj");
	BunnyOBJ->SetPosition(glm::vec4(0, 0, 0,1));
	BunnyOBJ->SetSphereOffset(glm::vec3(-1, -1, -1));
	BunnyOBJ->SetSphereRadius(2);
	BunnyOBJ->Initialize("../bin/data/models/stanford/", "Bunny.obj");
	basicPlane = new Plane3D(vertexData, indexData, "../bin/data/textures/four/four_diffuse.tga");
	//woodPlane = new Mesh3D(vertexData, indexData, "./bin/data/textures/four/four_diffuse.tga", "./bin/data/textures/four/four_normal.tga");
	m_texture = TextureLoader::LoadTexture("../bin/data/textures/four/four_diffuse.tga");
	return true;
}

bool Application::Shutdown()
{
	delete mCamera;
	delete basicPlane;
	delete BunnyOBJ;
	delete mGrid;
	Gizmos::destroy();
	DestroyWindow();
	return true;
}


void Application::Run()
{
	Startup();
	deltaTime = 0;
	prevTime = 0;
	glfwMakeContextCurrent(mWindow);
	//glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	while (glfwWindowShouldClose(mWindow) == false && glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		currTime = (float)glfwGetTime();
		deltaTime = currTime - prevTime;
		prevTime = currTime; //deltatime

		Update(deltaTime);
		Draw();
		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}
}

void Application::Draw()
{
	//first parse
	//draw shadow casting geometry
	BunnyOBJ->DrawVerts(mCamera->GetProjectionView());
	mGrid->Draw(mCamera->GetProjectionView());
	//basicPlane->Draw();

	//draw my shadows after they have been generated
	//DrawShadows();
	//draw the objects with the shadows
	BunnyOBJ->DrawVerts(mCamera->GetProjectionView());
	mGrid->DrawVerts(mCamera->GetProjectionView());
	//basicPlane->Draw();
}

void Application::Update(float deltatime)
{
	BunnyOBJ->Update(deltaTime);
	//glfwSetCursorPos(mWindow, (float)iWidth / 2, (float)iHeight / 2);
	mCamera->Update(mWindow, deltatime);
	//m_emitter->Update(deltatime, mCamera->GetWorldTransform());
}

int Application::GetHeight()
{
	return iHeight;
}

int Application::GetWidth()
{
	return iWidth;
}

FPSCamera* Application::getCamera()
{
	return mCamera;
}

bool Application::DestroyWindow()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();
	return true;
}
