#pragma once
#include <glm\ext.hpp>
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtx\transform.hpp>
#include "tiny_obj_loader.h"
#include "Moon.h"
#include "Grid.h"
//TODO:
//clean EVERYTHING
struct GLFWwindow;
class FPSCamera;
class Model3D;
class Plane3D;
class Grid;
class ParticleEmitter;

struct KeyFrame {
	glm::vec3 position;
	glm::quat rotation;
};


class Application
{

public:
	Application();
	~Application();

	bool Initialize();
	void MakeDefaultCam();
	virtual bool Startup();
	virtual bool Shutdown();

	void Run();
	virtual void Draw();
	virtual void Update(float deltatime);

	GLFWwindow* window;
	GLFWwindow* mWindow;

	int GetHeight();
	int GetWidth();

	FPSCamera* getCamera();
	virtual bool DestroyWindow();

	void InitializeShadows();
	void GenerateShadows();
	void DrawShadows();

	unsigned int m_texture;
private:
	ParticleEmitter* m_emitter;

	//Mesh3D* woodPlane;
	Model3D* BunnyOBJ;
	Plane3D* basicPlane;
	Grid* mGrid;
	//static unsigned int program;


	unsigned int mParticleShaders;
	unsigned int mBasicProgram;


protected:
	unsigned int mFBODepth;
	unsigned int mFBO;

	glm::vec3 mLightDirection;
	glm::mat4 mLightProjection;
	glm::mat4 mLightView;
	glm::mat4 mLightMatrix;

	unsigned int mGenerateShadow;
	unsigned int mUseShadow;

	FPSCamera* mCamera;
	unsigned int m_programID;
	//deltatime stuff
	float prevTime;
	float currTime;
	float deltaTime;

	int iHeight;
	int iWidth;

	bool mbGuiActive;

	glm::vec3 clearColour = glm::vec3(0.25f, 0.25f, 0.25f);
};

