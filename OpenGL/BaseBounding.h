#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>
#include <vector>

// Including the OpenGL & AIE Gizmos Libraries.
#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"
#include "AIE\Gizmos.h"
#include "glm\glm.hpp"
#include "glm/ext.hpp"
#include "glm/gtx/transform.hpp"

class BaseBounding
{
public:

	// Default constructor.
	BaseBounding();
	// Default destructor.
	virtual ~BaseBounding();

private:

protected:

	// Sphere radius.
	GLfloat m_fRadius;

	// Sphere centre.
	glm::vec3 m_v3Centre;
	// Minimum point.
	glm::vec3 m_v3Minimum;
	// Maximum point.
	glm::vec3 m_v3Maximum;

};