#include "BaseCamera.h"



BaseCamera::BaseCamera()
{

}


BaseCamera::~BaseCamera()
{
}

void BaseCamera::Update(float deltatime)
{

	worldTransform = glm::inverse(viewTransform);
	UpdateProjectionViewTransform();

}

void BaseCamera::setPerspective(float aFOV, float afAspectRatio, float near, float far)
{
	projectionTransform = glm::perspective(aFOV, afAspectRatio, near, far);
}

void BaseCamera::setLookat(glm::vec3 aFrom, glm::vec3 aTo, glm::vec3 aUp)
{
	viewTransform = glm::lookAt(aFrom, aTo ,aUp);
}

void BaseCamera::setPosition(glm::vec3 aPosition)
{
	worldTransform[3] = glm::vec4(aPosition, 1);
}

glm::mat4 BaseCamera::GetWorldTransform()
{
	return glm::inverse(viewTransform);
}

glm::mat4 BaseCamera::GetView()
{
	return viewTransform;
}

glm::mat4 BaseCamera::GetProjection()
{
	return projectionTransform;
}

glm::mat4 BaseCamera::GetProjectionView()
{
	projectionViewTransform = projectionTransform * viewTransform;
	return projectionViewTransform;
}

void BaseCamera::UpdateProjectionViewTransform()
{
	projectionViewTransform = projectionTransform * viewTransform;
}
