#include "BoundingSphere.h"

// Default constructor.
BoundingSphere::BoundingSphere()
{
	Reset();
}

// Default destructor.
BoundingSphere::~BoundingSphere()
{

}

// Resets member variables that are used.
void BoundingSphere::Reset()
{
	m_v3Centre.x = m_v3Centre.y = m_v3Centre.z = 0;
	m_fRadius = 0;
}

void BoundingSphere::Fit(const std::vector<glm::vec3>& a_vPoints)
{
	m_v3Minimum.x = m_v3Minimum.y = m_v3Minimum.z = 1e37f;
	m_v3Maximum.x = m_v3Maximum.y = m_v3Maximum.z = -1e37f;

	for (auto& aP : a_vPoints)
	{
		if (aP.x < m_v3Minimum.x)
		{
			m_v3Minimum.x = aP.x;
		}
		if (aP.y < m_v3Minimum.y)
		{
			m_v3Minimum.y = aP.y;
		}
		if (aP.z < m_v3Minimum.z)
		{
			m_v3Minimum.z = aP.z;
		}
		if (aP.x > m_v3Maximum.x)
		{
			m_v3Maximum.x = aP.x;
		}
		if (aP.y > m_v3Maximum.y)
		{
			m_v3Maximum.y = aP.y;
		}
		if (aP.z > m_v3Maximum.z)
		{
			m_v3Maximum.z = aP.z;
		}
	}

	m_v3Centre = (m_v3Minimum + m_v3Maximum) * 0.5f;
	m_fRadius = glm::distance(m_v3Minimum, m_v3Centre);
}

// Set sphere's radius.
void BoundingSphere::SetRadius(GLfloat a_fRadius)
{
	m_fRadius = a_fRadius;
}

// Set sphere's centre.
void BoundingSphere::SetCentre(glm::vec3 a_v3Centre)
{
	m_v3Centre = a_v3Centre;
}