#pragma once

#include "BaseBounding.h"

class BoundingSphere : public BaseBounding
{
public:

	// Default constructor.
	BoundingSphere();
	// Default destructor.
	~BoundingSphere();

	// Resets member variables that are used.
	void Reset();

	void Fit(const std::vector<glm::vec3>& a_vPoints);
	// Set sphere's radius.
	void SetRadius(GLfloat a_fRadius);
	// Set sphere's centre.
	void SetCentre(glm::vec3 a_v3Centre);

	// Return sphere's radius.
	float GetRadius() const { return m_fRadius; };

	// Return sphere's centre.
	glm::vec3 GetCentre() const { return m_v3Centre; };

private:

protected:

};