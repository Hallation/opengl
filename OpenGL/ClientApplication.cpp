#include "ClientApplication.h"
#include "IncludeHelpers.h"
#include "FPSCamera.h"
#include "GLFW\glfw3.h"
#include "stb\stb_image.h"
#include "ImGUIImplement.h"
#include "AIE\Gizmos.h"
#include "Model3D.h"
#include "Grid.h"
#include "NetworkClient.h"

#include <iostream>
#include <thread>
#include "GameMessages.h"

ClientApplication::ClientApplication()
{
}


ClientApplication::~ClientApplication()
{
}

bool ClientApplication::Startup()
{
	mbGuiActive = false;
	//make a camera
	MakeDefaultCam();
	//initialize glfw
	Initialize();
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	//load my shaders
	m_programID = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/basicVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");
	//initialize gui
	ImGui_ImplGlfwGL3_Init(mWindow, true);
	//gizmo object at position 0 and colour of white
	mGizmoObject =
	{
		glm::vec4(0),
		glm::vec4(1)
	};
	mModel3D = new Model3D();
	mModel3D->SetPosition(glm::vec4(0, 0, 0, 1));
	mModel3D->SetSphereOffset(glm::vec3(0));
	mModel3D->SetSphereRadius(5);
	mModel3D->Initialize("../bin/data/models/soulspear/", "soulspear.obj");
	mModel3D->LoadShaders("../bin/data/shaders/vertShaders/basicVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");
	mModel3D->LoadTexture("../bin/data/models/soulspear/soulspear_diffuse.tga");

	mGrid = new Grid();
	mGrid->SetPosition(glm::vec4(-15.f, 0.f, -10.f, 1));
	mGrid->Generate(64, 64);
	mGrid->LoadShaders("../bin/data/shaders/vertShaders/basicVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");

	mGizmoObject.mvPosition = glm::vec4(0);
	mGizmoObject.mvColour = glm::vec4(1);

	mNetworkClient = new NetworkClient();
	//RakNet::BitStream bs;
	//bs.Write((RakNet::MessageID)GameMessages::ID_CLIENT_CLIENT_DATA);
	//bs.Write((char*)&mGizmoObject, sizeof(GameObject));
	//RakNet::RakPeerInterface::GetInstance()->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	
	Gizmos::create(10000, 10000, 10000, 10000);
	return false;
}

bool ClientApplication::Shutdown()
{
	mNetworkClient->Disconnect();
	delete mCamera;
	delete mModel3D;
	delete mGrid;
	delete mNetworkClient;
	Gizmos::destroy();
	DestroyWindow();
	ImGui_ImplGlfwGL3_Shutdown();
	return false;
}

void Test()
{
	std::cout << "Hello" << std::endl;
}
void ClientApplication::Draw()
{
	Gizmos::clear();
	ImGui_ImplGlfwGL3_NewFrame();
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glUseProgram(m_programID);
	int loc = glGetUniformLocation(m_programID, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &mCamera->GetProjectionView()[0][0]);

	mModel3D->Draw(mCamera->GetProjectionView(), mCamera->GetWorldTransform(), glm::vec4(1), glm::vec4(0, 1, 0, 1));
	mGrid->Draw(mCamera->GetProjectionView());

	//update the other clients shit on mine;

	if (!mbGuiActive)
	{
		ImGui::Begin("Camera Paused");
		ImGui::SetWindowSize(ImVec2(400, 250));
		ImGui::InputFloat("MovementSpeed", mModel3D->MovementSpeed());
		ImGui::InputText("IP Address", ipAddress, 15);
		ImGui::InputInt("Port", &PORT);
		if (!mNetworkClient->Connected())
		{
			if (ImGui::Button("Connect"))
			{
				mNetworkClient->CreateConnection(ipAddress, PORT);
			}
		}
		if (ImGui::Button("Disconnect"))mNetworkClient->Disconnect();
		ImGui::End();
	}
	ImGui::Render();

	Gizmos::draw(mCamera->GetProjectionView());
}

void ClientApplication::Update(float deltatime)
{
	//send any input information
	GameObject temp;
	//check for any packets that come through
	mModel3D->Update(deltatime);

	if (glfwGetKey(mWindow, GLFW_KEY_UP) == GLFW_PRESS)
	{
		temp.mvPosition.y += *mModel3D->MovementSpeed() * deltatime;
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		temp.mvPosition.y -= *mModel3D->MovementSpeed()* deltatime;
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		temp.mvPosition.x -= *mModel3D->MovementSpeed() * deltatime;
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		temp.mvPosition.x += *mModel3D->MovementSpeed() * deltatime;
		//mNetworkClient->sendClientGameObject(temp);
	}

	if (glfwGetKey(mWindow, GLFW_KEY_F9) == GLFW_PRESS)
	{
		mbGuiActive = true;
	}
	if (glfwGetKey(mWindow, GLFW_KEY_F8) == GLFW_PRESS)
	{
		mbGuiActive = false;
	}

	if (mbGuiActive)
	{
		mCamera->Update(mWindow, deltatime);
	}

	mNetworkClient->Run();
}
