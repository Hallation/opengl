#pragma once
#include "Application.h"
#include "structs.h"

#include <RakPeerInterface.h>
#include <unordered_map>
class Model3D;
class Grid;
class NetworkClient;
class ClientApplication : public Application
{
public:
	ClientApplication();
	~ClientApplication();
	bool Startup();
	bool Shutdown();

	void Draw();
	void Update(float deltatime);
private:
	int PORT = 9999;
	char ipAddress[15] = "127.0.0.1";
	Model3D* mModel3D;
	Grid* mGrid;

	GameObject mGizmoObject;

	NetworkClient* mNetworkClient;
	std::unordered_map<int, Model3D> m_otherClientsModels;
};

