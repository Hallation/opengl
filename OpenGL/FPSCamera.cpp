#include "FPSCamera.h"
#include "GLFW\glfw3.h"
void Rotate(glm::quat* a_quat, float theta, glm::vec3 a_axis);
FPSCamera::FPSCamera()
{
	worldTransform = glm::mat4(1);
	viewTransform = glm::mat4(1);
	projectionTransform = glm::mat4(1);
	projectionViewTransform = glm::mat4(1);

	mfRadius = 0;
	mfCursorX = 0;
	mfCursorY = 0;
	mfLastCursorX = 0;
	mfLastCursorY = 0;
	mfPitch = 0;
	mfYaw = 0;
	mfRoll = 0;

	mbFirstMouseMovement = false;
}


FPSCamera::~FPSCamera()
{
}

void FPSCamera::SetWorldTransform(glm::vec4 avPosition)
{
	worldTransform[3] = avPosition;
	viewTransform = glm::inverse(worldTransform);
	UpdateProjectionViewTransform();
}

void FPSCamera::SetviewTransform(glm::vec3 avPosition, glm::vec3 avTarget, glm::vec3 aUpAxis)
{
	viewTransform = glm::lookAt(avPosition, avTarget, aUpAxis);
	worldTransform = glm::inverse(viewTransform);
	UpdateProjectionViewTransform();
}

void FPSCamera::SetProjectionTransform(float aFOV, float afAspectRatio, float aNear, float aFar)
{
	projectionTransform = glm::perspective(aFOV, afAspectRatio, aNear, aFar);
}

void FPSCamera::Update(GLFWwindow* aWindow, float deltatime)
{

	glfwGetCursorPos(aWindow, &mfCursorX, &mfCursorY);
	if (mbFirstMouseMovement)
	{
		mfCursorX = 800.f;
		mfCursorY = 450.f;
		mbFirstMouseMovement = false;
	}
	GLdouble mDeltaX = (800 - mfCursorX) * 0.05f;
	GLdouble mDeltaY = (450 - mfCursorY) * 0.05f;

	glfwSetCursorPos(aWindow, 800, 450);

	glm::vec4 mvUp = glm::inverse(worldTransform) * glm::vec4(0, 1, 0, 0);

	glm::quat rotation;
	Rotate(&rotation, (float)-mDeltaX * deltatime, glm::vec3(mvUp[0], mvUp[1], mvUp[2]));
	viewTransform = glm::toMat4(rotation)* viewTransform;
	Rotate(&rotation, (float)-mDeltaY * deltatime, glm::vec3(1, 0, 0));

	viewTransform = glm::toMat4(rotation) * viewTransform;

	worldTransform = glm::inverse(viewTransform);

	GLfloat mfSpeed = 20.0f * deltatime;

	if (glfwGetKey(aWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		worldTransform[3] -= worldTransform[2] * mfSpeed;
	}

	if (glfwGetKey(aWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[2] * mfSpeed;
	}

	if (glfwGetKey(aWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		worldTransform[3] -= worldTransform[0] * mfSpeed;
	}

	if (glfwGetKey(aWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		worldTransform[3] += worldTransform[0] * mfSpeed;
	}

	if (glfwGetKey(aWindow, GLFW_KEY_UP))
		worldTransform[3] -= worldTransform[2] * (mfSpeed * deltatime);
	if (glfwGetKey(aWindow, GLFW_KEY_DOWN))
		worldTransform[3] += worldTransform[2] * (mfSpeed * deltatime);

	worldTransform[3][3] = 1.0f;
	viewTransform = glm::inverse(worldTransform);
	UpdateProjectionViewTransform();
}

void Rotate(glm::quat * a_quat, float theta, glm::vec3 a_axis)
{
	a_quat->x = a_axis.x * sin(theta / 2);
	a_quat->y = a_axis.y * sin(theta / 2);
	a_quat->z = a_axis.z * sin(theta / 2);
	a_quat->w = cos(theta / 2);
}
