#pragma once
#include "BaseCamera.h"

struct GLFWwindow;
class FPSCamera :
	public BaseCamera
{
public:
	FPSCamera();
	~FPSCamera();

	void SetWorldTransform(glm::vec4 avPosition);
	void SetviewTransform(glm::vec3 avPosition, glm::vec3 avTarget, glm::vec3 aUpAxis);
	void SetProjectionTransform(float aFOV, float afAspectRatio, float aNear, float aFar);
	void Update(GLFWwindow* aWindow, float deltatime);

protected:
	glm::vec3 mvPosition;
	glm::vec3 mvTarget;
	glm::vec3 mvDirection;
	glm::vec3 mvUpAxis;
	glm::vec3 mvRight;
	glm::vec3 mvUp;
	glm::vec3 mvFront;
	glm::vec3 mvMouseDirection;

};

