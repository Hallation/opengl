#include "FrustrumCulling.h"

// Default constructor.
FrustrumCulling::FrustrumCulling()
{

}

// Default destructor.
FrustrumCulling::~FrustrumCulling()
{

}

// Initialises planes for culling.
void FrustrumCulling::SetFrustrumPlanes(const glm::mat4& a_m4ProjectionViewMatrix, glm::vec4* a_v4Planes)
{
	// Right side.
	a_v4Planes[0] = glm::vec4(a_m4ProjectionViewMatrix[0][3] - a_m4ProjectionViewMatrix[0][0],
							  a_m4ProjectionViewMatrix[1][3] - a_m4ProjectionViewMatrix[1][0],
							  a_m4ProjectionViewMatrix[2][3] - a_m4ProjectionViewMatrix[2][0],
							  a_m4ProjectionViewMatrix[3][3] - a_m4ProjectionViewMatrix[3][0]);

	// Left side.
	a_v4Planes[1] = glm::vec4(a_m4ProjectionViewMatrix[0][3] + a_m4ProjectionViewMatrix[0][0],
							  a_m4ProjectionViewMatrix[1][3] + a_m4ProjectionViewMatrix[1][0],
							  a_m4ProjectionViewMatrix[2][3] + a_m4ProjectionViewMatrix[2][0],
							  a_m4ProjectionViewMatrix[3][3] + a_m4ProjectionViewMatrix[3][0]);

	// Top.
	a_v4Planes[2] = glm::vec4(a_m4ProjectionViewMatrix[0][3] - a_m4ProjectionViewMatrix[0][1],
							  a_m4ProjectionViewMatrix[1][3] - a_m4ProjectionViewMatrix[1][1],
							  a_m4ProjectionViewMatrix[2][3] - a_m4ProjectionViewMatrix[2][1],
							  a_m4ProjectionViewMatrix[3][3] - a_m4ProjectionViewMatrix[3][1]);

	// Bottom.
	a_v4Planes[3] = glm::vec4(a_m4ProjectionViewMatrix[0][3] + a_m4ProjectionViewMatrix[0][1],
							  a_m4ProjectionViewMatrix[1][3] + a_m4ProjectionViewMatrix[1][1],
							  a_m4ProjectionViewMatrix[2][3] + a_m4ProjectionViewMatrix[2][1],
							  a_m4ProjectionViewMatrix[3][3] + a_m4ProjectionViewMatrix[3][1]);

	// Far.
	a_v4Planes[4] = glm::vec4(a_m4ProjectionViewMatrix[0][3] - a_m4ProjectionViewMatrix[0][2],
							  a_m4ProjectionViewMatrix[1][3] - a_m4ProjectionViewMatrix[1][2],
							  a_m4ProjectionViewMatrix[2][3] - a_m4ProjectionViewMatrix[2][2],
							  a_m4ProjectionViewMatrix[3][3] - a_m4ProjectionViewMatrix[3][2]);

	// Near.
	a_v4Planes[5] = glm::vec4(a_m4ProjectionViewMatrix[0][3] + a_m4ProjectionViewMatrix[0][2],
							  a_m4ProjectionViewMatrix[1][3] + a_m4ProjectionViewMatrix[1][2],
							  a_m4ProjectionViewMatrix[2][3] + a_m4ProjectionViewMatrix[2][2],
							  a_m4ProjectionViewMatrix[3][3] + a_m4ProjectionViewMatrix[3][2]);

	// Plane normalisation based on length of normal.
	for (int i= 0; i< 6; ++i)
	{
		float length = glm::length(glm::vec3(a_v4Planes[i]));
		a_v4Planes[i] /= length;
	}
}

// Checks if camera can view the object or should cull it.
GLboolean FrustrumCulling::SphereCullCheck(glm::mat4 a_m4ProjectionViewMatrix, glm::vec3 a_SphereCentre, GLfloat a_fSphereRadius)
{
	// Initialises planes for culling.
	glm::vec4 v4Planes[6];
	SetFrustrumPlanes(a_m4ProjectionViewMatrix, v4Planes);

	// Perform culling checks.
	for (GLint iCount = 0; iCount < 6; ++iCount)
	{
		GLfloat d = glm::dot(glm::vec3(v4Planes[iCount]), a_SphereCentre) + v4Planes[iCount].w;
		/*
		float d = glm::dot(vec3(planes[i]), sphere.centre) + planes[i].w;
		*/
		// Not visible. Do not render it.
		if (d < -a_fSphereRadius)
		{
			std::cout << "False." << std::endl;
			return false;
		}
		// Almost visible. Render it.
		else if (d < a_fSphereRadius)
		{
			std::cout << "AlmostTrue." << std::endl;
			return true;
		}
		// Visible. Render it.
		else
		{
			std::cout << "True." << std::endl;
			return true;
		}
	}
	return false;
}