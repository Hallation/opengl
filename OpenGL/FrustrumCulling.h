#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <iostream>

// Including the OpenGL & AIE Gizmos Libraries.
#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"
#include "AIE\Gizmos.h"
#include "glm\glm.hpp"
#include "glm/ext.hpp"
#include "glm/gtx/transform.hpp"

class FrustrumCulling
{
public:

	// Default constructor.
	FrustrumCulling();
	// Default destructor.
	~FrustrumCulling();

	// Initialises planes for culling.
	void SetFrustrumPlanes(const glm::mat4& a_m4ProjectionViewMatrix, glm::vec4* a_v4Planes);
	// Checks if camera can view the object or should cull it.
	GLboolean SphereCullCheck(glm::mat4 a_m4ProjectionViewMatrix, glm::vec3 a_SphereCentre, GLfloat a_fSphereRadius);

private:

	// Frustrum culling planes.
	glm::vec4 m_v4Plane;

protected:

};