#include "glm\ext.hpp"
#include <string>
struct OBJVertex
{
	float x, y, z, w; //4 0
	float nx, ny, nz , nw; //4 16
	float u, v; //2 32
};

struct OpenGLInfo
{
	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_VBO1;
	unsigned int m_faceCount;
};

struct Vertex {
	float x, y, z, w; //4 +0
	float nx, ny, nz, nw; //4 +16
	float tx, ty, tz, tw; //4 +32
	float s, t; //2 +48
};

