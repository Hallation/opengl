#include "Grid.h"
#include "gl_core_4_4.h"
#include "IncludeHelpers.h"
#include "stb\stb_image.h"
Grid::Grid()
{

}

Grid::Grid(int cols, int rows)
{
	mNumberOfIndicies = 0;
	Vertex* aoVerticies = new Vertex[rows * cols];
	
	glm::vec3 position(0);
	//genereate verticies for our grid
	mNumberOfIndicies = (rows - 1) * (cols - 1) * 6;
	for (int r = 0; r < rows; ++r)
	{
		for (int c = 0; c < cols; ++c)
		{
			//Verts
			aoVerticies[r* cols + c].x = (float)c - (cols * 0.5f);
			aoVerticies[r* cols + c].y = 0 * position.y;
			aoVerticies[r* cols + c].z = (float)r - (rows * 0.5f);
			aoVerticies[r* cols + c].w = 1;

			//Normals
			glm::vec3 colour = glm::vec3(sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
			aoVerticies[r * cols + c].nx = colour.x;
			aoVerticies[r * cols + c].ny = colour.y;
			aoVerticies[r * cols + c].nz = colour.z;
			aoVerticies[r * cols + c].nw = 1;

			//UVs
			aoVerticies[r * cols + c].s = (float)aoVerticies[r * cols +c].x / rows; 
			aoVerticies[r * cols + c].t = 1 - (float)aoVerticies[r * cols +c].y / cols;
		}
	}
	//calculate indices
	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];
	unsigned int index = 0;
	for (int r = 0; r < (rows - 1); ++r)
	{
		for (int c = 0; c < (cols -1); ++c)
		{
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);

			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	//for (int i = 0; i < (rows * cols); ++i)
	//{
	//	aoVerticies[i].x -= rows *0.5f;
	//	aoVerticies[i].z -= cols *0.5f;
	//}
	//generate UVS for the grid
	/*
	get the length of verts
	make an array of the size of verts
	*/
	//genereate VAO;
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	//create and bind VBO
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(GridVertex), aoVerticies, GL_STATIC_DRAW);

	//create and bind the IBO
	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //position
	glEnableVertexAttribArray(1); //tex coords
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(GridVertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GridVertex), (char*)0+48);

	//int dims = 64;
	//float* perlinData = new float[dims * dims];
	////scale our points down
	//int octaves = 6;
	//float scale = (1.0f / dims) * 3;
	////use glm perlin function to create noise
	//for (int x = 0; x < 64; ++x)
	//{
	//	for (int y = 0; y < 64; ++y)
	//	{
	//		float amplitude = 1.f;
	//		float persistence = 0.3f;
	//		perlinData[y * dims + x] = 0;
	//		for (int o = 0; o < octaves; ++o)
	//		{
	//			float freq = powf(2, (float)o);
	//			float perlinSample = glm::perlin(glm::vec2((float)x, (float)y * scale * freq) * 0.5f + 0.5f);
	//			perlinData[y * dims + x] += perlinSample * amplitude;
	//			amplitude += persistence;
	//		}
	//		//generate perlin noise
	//	}
	//}

	////make a texture containing the perlin noise
	//glGenTextures(1, &m_texture);
	//glBindTexture(GL_TEXTURE_2D, m_texture);

	//glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlinData);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	////clamp the texture to the edge
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] aoVerticies;
	delete[] auiIndices;
	//delete perlinData;
}

Grid::~Grid()
{
}

void Grid::LoadShaders(const char * vertShader, const char * fragShader)
{
	m_programID = ShaderLoader::LoadShader(vertShader, fragShader);
}

void Grid::SetPosition(glm::vec4 avPosition)
{
	mvPosition = avPosition;
}

void Grid::Draw(glm::mat4 projectionView)
{
	glUseProgram(m_programID);
	int loc = glGetUniformLocation(m_programID, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &(projectionView[0][0]));
	glUniform1i(loc, 0);
	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, mNumberOfIndicies, GL_UNSIGNED_INT, 0);
}

void Grid::DrawVerts(glm::mat4 projectionView)
{
	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, mNumberOfIndicies, GL_UNSIGNED_INT, 0);
}

void Grid::Generate(int rows, int cols)
{
	mNumberOfIndicies = 0;
	Vertex* aoVerticies = new Vertex[rows * cols];

	glm::vec3 position(0);
	//genereate verticies for our grid
	mNumberOfIndicies = (rows - 1) * (cols - 1) * 6;
	for (int r = 0; r < rows; ++r)
	{
		for (int c = 0; c < cols; ++c)
		{
			//vec4 ( x, y z, float r
			aoVerticies[r* cols + c].x = (float)c + mvPosition.x;
			aoVerticies[r* cols + c].y = 0 + mvPosition.y;
			aoVerticies[r* cols + c].z = (float)r + mvPosition.z;
			aoVerticies[r* cols + c].w = 1;
			//create some arbitrary colour
			glm::vec3 colour = glm::vec3(sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
			aoVerticies[r * cols + c].nx = colour.x;
			aoVerticies[r * cols + c].ny = colour.y;
			aoVerticies[r * cols + c].nz = colour.z;
			aoVerticies[r * cols + c].nw = 1;

			aoVerticies[r * cols + c].s = (float)aoVerticies[r * cols + c].x / rows;
			aoVerticies[r * cols + c].t = 1 - (float)aoVerticies[r * cols + c].y / cols;
		}
	}
	//calculate indices
	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];
	unsigned int index = 0;
	for (int r = 0; r < (rows - 1); ++r)
	{
		for (int c = 0; c < (cols - 1); ++c)
		{
			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);

			auiIndices[index++] = r * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	//for (int i = 0; i < (rows * cols); ++i)
	//{
	//	aoVerticies[i].x -= rows *0.5f;
	//	aoVerticies[i].z -= cols *0.5f;
	//}
	//generate UVS for the grid
	/*
	get the length of verts
	make an array of the size of verts
	*/
	//genereate VAO;
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	//create and bind VBO
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(GridVertex), aoVerticies, GL_STATIC_DRAW);

	//create and bind the IBO
	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //position
	glEnableVertexAttribArray(2); //tex coords
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(GridVertex), 0);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GridVertex), (char*)0 + 48);

	//int dims = 64;
	//float* perlinData = new float[dims * dims];
	////scale our points down
	//int octaves = 6;
	//float scale = (1.0f / dims) * 3;
	////use glm perlin function to create noise
	//for (int x = 0; x < 64; ++x)
	//{
	//	for (int y = 0; y < 64; ++y)
	//	{
	//		float amplitude = 1.f;
	//		float persistence = 0.3f;
	//		perlinData[y * dims + x] = 0;
	//		for (int o = 0; o < octaves; ++o)
	//		{
	//			float freq = powf(2, (float)o);
	//			float perlinSample = glm::perlin(glm::vec2((float)x, (float)y * scale * freq) * 0.5f + 0.5f);
	//			perlinData[y * dims + x] += perlinSample * amplitude;
	//			amplitude += persistence;
	//		}
	//		//generate perlin noise
	//	}
	//}

	////make a texture containing the perlin noise
	//glGenTextures(1, &m_texture);
	//glBindTexture(GL_TEXTURE_2D, m_texture);

	//glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlinData);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	////clamp the texture to the edge
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] aoVerticies;
	delete[] auiIndices;
	//delete perlinData;
}
