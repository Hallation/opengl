#pragma once
#include "glm\ext.hpp"
struct Vertex;
struct GridVertex
{
	glm::vec4 position; //4
	glm::vec4 normals; //4
	glm::vec4 colour; //4
	float s, t; //2
};

/*
float x, y, z, w; //4 +0
float nx, ny, nz, nw; //4 +16
float tx, ty, tz, tw; //4 +32
float s, t; //2 +48

*/
class Grid
{
public:
	Grid();
	Grid(int cols, int rows);
	~Grid();

	void LoadShaders(const char* vertShader, const char* fragShader);
	void SetPosition(glm::vec4 avPosition);
	void Draw(glm::mat4 projectionView);
	void DrawVerts(glm::mat4 projectionView);
	void Generate(int xSize, int ySize);
private:
	glm::vec4 mvPosition;
	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
	unsigned int mNumberOfIndicies;

	unsigned int m_programID;
	float* perlinData;
	unsigned int m_texture;
};

