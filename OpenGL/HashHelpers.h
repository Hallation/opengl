#pragma once
#include <sha.h>
#include <hex.h>
template <class T>
class HashHelpers
{
public:
	HashHelpers() {};
	~HashHelpers() {};

	std::string generateHash(std::string source)
	{
		std::cout << "Hashing being " << std::endl;
		CryptoPP::SHA1 hash;
		byte digest[CryptoPP::SHA1::DIGESTSIZE];
		hash.CalculateDigest(digest, (const byte*)source.c_str(), source.size());

		std::string output;
		CryptoPP::HexEncoder encoder;
		CryptoPP::StringSink sinkTest = CryptoPP::StringSink(output);
		encoder.Attach(new CryptoPP::StringSink(output));
		encoder.Put(digest, sizeof(digest));
		encoder.MessageEnd();
		std::cout << "----------------------------------------------------------------\n";
		std::cout << "End of hashing test " << std::endl;
		return output;
	}

	std::string generateHash(T source)
	{
		std::cout << "Begin hashing" << std::endl;
		CryptoPP::SHA1 hash;
		byte digest[CryptoPP::SHA1::DIGESTSIZE];
		hash.CalculateDigest(digest, (const byte*)source, sizeof(source));
		
		std::string output;
		CryptoPP::HexEncoder encoder;
		CryptoPP::StringSink sinkTest = CryptoPP::StringSink(output);
		encoder.Attach(new CryptoPP::StringSink(output));
		encoder.Put(digest, sizeof(digest));
		encoder.MessageEnd();
		std::cout << "End of hasing " << std::endl;
		return output;

	}
};

