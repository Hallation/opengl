#pragma once
#include "glm\ext.hpp"
class Light
{
public:
	Light();
	Light(glm::vec4 aPosition, glm::vec3 aLightDir, glm::vec3 aLightColour);
	~Light();
	//base light

	glm::vec4 Position()
	{
		return mPosition;
	}
	glm::vec3 Direction()
	{
		return mDirection;
	}
	glm::vec3 LightColour()
	{
		return mLightColour;
	}
	
	void Update(float deltatime);
private:
	glm::vec4 mPosition;
	glm::vec3 mDirection;
	glm::vec3 mLightColour;

};

