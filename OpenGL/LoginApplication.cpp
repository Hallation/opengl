#include "LoginApplication.h"

#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"

#include "FPSCamera.h"
#include "IncludeHelpers.h"
#include "LoginNetworkClient.h"
#include "ImGUIImplement.h"

#include <string>
#include <iostream>
#include <thread>
#include <chrono>
LoginApplication::LoginApplication()
{
}


LoginApplication::~LoginApplication()
{

}

bool LoginApplication::Startup()
{
#pragma region startup
	//define my camera
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);

	if (!Initialize())
	{
		std::cout << "something broke in initilizaiton";
		return false;
	}

	InitializeShadows();
#pragma region Shaders
	//basic lighting [0]
	programID = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/lightingVS.glsl", "../bin/data/shaders/fragShaders/lightingFS.glsl");
	glClearColor(0.25f, 0.25f, 0.25f, 1); //clear color
	glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	Gizmos::create(100000, 100000, 100000, 100000);

	ImGui_ImplGlfwGL3_Init(mWindow, true);
	mClient = new LoginNetworkClient();
	mClient->CreateConnection("127.0.0.1", 9999);
	mClient->SetLoginIP("127.0.0.1", 9999);
	mClient->SetMainServer("127.0.0.1", 7777);
	return true;
}

bool LoginApplication::Shutdown()
{
	mClient->DisconnectLogOut();
	delete mCamera;
	delete mClient;

	Gizmos::destroy();
	DestroyWindow();
	ImGui_ImplGlfwGL3_Shutdown();

	return false;
}

void LoginApplication::Update(float deltatime)
{
	mClient->Run();
	mClient->CheckForLogIn();

	float MovementSpeed = 5.f;
	if (glfwGetKey(mWindow, GLFW_KEY_UP) == GLFW_PRESS)
	{
		mClient->mGameObject.mvPosition.y += MovementSpeed * deltatime;
		mClient->UpdateObject();
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		mClient->mGameObject.mvPosition.y -= MovementSpeed* deltatime;
		mClient->UpdateObject();
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		mClient->mGameObject.mvPosition.x -= MovementSpeed * deltatime;
		mClient->UpdateObject();
		//mNetworkClient->sendClientGameObject(temp);
	}
	if (glfwGetKey(mWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		mClient->mGameObject.mvPosition.x += MovementSpeed * deltatime;
		mClient->UpdateObject();
		//mNetworkClient->sendClientGameObject(temp);
	}

}

void LoginApplication::Draw()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	ImGui_ImplGlfwGL3_NewFrame();
	Gizmos::clear();
	ImGui::Begin("Menu");

	if (!mClient->Connected())
	{
		ImGui::Text("I am not connected to a server");
		if (ImGui::Button("Connect"))
		{
			mClient->ConnectToLogin();
		}
	}
	else if (!mClient->IsLoggedIn())
	{
		ImGui::SetWindowSize(ImVec2(400, 250));
		ImGui::InputText("Username", (char*)&username, 32);
		ImGui::InputText("password", (char*)&password, 32);

		if (ImGui::Button("Login"))
		{
			//connect to the server for login services

			User temp;
			strcpy_s(temp.username, username);
			strcpy_s(temp.password, password);

			strcpy_s(username, "");
			strcpy_s(password, "");
			mClient->onLoginAttempt(temp);
		}

		if (ImGui::Button("Register"))
		{
			User temp;
			strcpy_s(temp.username, username);
			strcpy_s(temp.password, password);

			strcpy_s(username, "");
			strcpy_s(password, "");
			mClient->onRegistrationAttempt(temp);
		}
	}
	else if (mClient->mbConnectedToMain && mClient->miClientID > 0) //If logged in
	{
		std::string loginText = "Logged in as " + std::string(mClient->mUser.username);
		ImGui::Text(loginText.c_str());
		ImGui::Text("Close the applicaiton to logout");

		if (ImGui::Button("Disconnect"))
		{
			mClient->LogOut();
			//TODO disconnect from the game server and reconnect to the login server
		}
	}

	ImGui::End();

	ImGui::Render();
	
	//draw any of the other clients, and set their sphere to be white
	if (mClient->IsLoggedIn() && mClient->mbConnectedToMain) 
	{
		for (auto it : mClient->mClientGameObjects)
		{
			Gizmos::addSphere(glm::vec3(it.second.mvPosition), 0.5f, 30, 30, glm::vec4(1));
		}
		//my sphere will always be red
		Gizmos::addSphere(glm::vec3(mClient->mGameObject.mvPosition), 0.5f, 30, 30, glm::vec4(255, 0, 0, 255));
		Gizmos::draw(mCamera->GetProjectionView());
	}
	
}
