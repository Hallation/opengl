#pragma once
#include "Application.h"

class LoginNetworkClient;
class LoginApplication :
	public Application
{
public:
	LoginApplication();
	~LoginApplication();


	bool Startup();
	bool Shutdown();
	void Update(float deltatime);
	void Draw();

private:
	unsigned int programID;
	LoginNetworkClient* mClient;

	char username[32]{ "" };
	char password[32]{ "" };
};

