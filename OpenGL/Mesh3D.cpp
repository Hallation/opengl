﻿#include "Mesh3D.h"
#include "Application.h"
#include "Camera.h"
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#include <iostream>
#include <string>
#if !defined TINYOBJLOADER_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#endif // !TINYOBJLOADER_IMPLEMENTATION
#if !defined STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb\stb_image.h"
#endif // !STB_IMAGE_IMPLMENTATION


Mesh3D::Mesh3D(Vertex * verticies, unsigned int numVerticies)
{

}
//from the original texture tutorial, this loads any texture. diffuse has to be defined, the normal, displacement and specular are null by default
Mesh3D::Mesh3D(Vertex vertexData[], unsigned int indexData[], const char * diffuseTex, const char * normalMap, const char * displacement, const char * specular)
{
	//this determines what draw call to use
	m_meshType = UserDefined;
	int imageWidth;
	int imageHeight;
	int imageFormat;
	unsigned char* data = stbi_load(diffuseTex, &imageWidth, &imageHeight, &imageFormat, STBI_default);
	glGenTextures(1, &m_texture); //generate our texture
	glBindTexture(GL_TEXTURE_2D, m_texture); //make sure it goes to the right position
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data); //load the texture data
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
	stbi_image_free(data);

	//sanity checks
	if (normalMap != nullptr) {
		data = stbi_load(normalMap, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_normal);
		glBindTexture(GL_TEXTURE_2D, m_normal);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (displacement != nullptr) {
		data = stbi_load(displacement, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_displacement);
		glBindTexture(GL_TEXTURE_2D, m_displacement);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (specular != nullptr) {
		data = stbi_load(specular, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_specular);
		glBindTexture(GL_TEXTURE_2D, m_specular);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}


	//genereate VAO;
	glGenVertexArrays(1, &m_glInfo.m_VAO);
	glBindVertexArray(m_glInfo.m_VAO);

	//create and bind VBO
	glGenBuffers(1, &m_glInfo.m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_glInfo.m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glEnableVertexAttribArray(1); //texcoord
	glEnableVertexAttribArray(2); //normals
	glEnableVertexAttribArray(3); //tangents
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); //position
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 16); //norms
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 48); //tex coords
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 32); //tangents
	//free everything
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}



Mesh3D::Mesh3D(const char* objLocation, const char* objName)
{
	//load the obj file, set mesh type to obj for different type of draw call. generate the opengl buffers
	m_meshType = OBJ;
	tinyobj::attrib_t attribs;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;
	std::string obj(objLocation);
	obj += objName;
	tinyobj::LoadObj(&attribs, &shapes, &materials, &err, obj.c_str(), objLocation);
	std::cout << err << std::endl;
	CreateOpenGLBuffers(attribs, shapes);
}


Mesh3D::~Mesh3D()
{
}

//once the obj has been loaded, bind the data to the gl buffers
void Mesh3D::CreateOpenGLBuffers(tinyobj::attrib_t & attribs, std::vector<tinyobj::shape_t>& shapes)
{
	glInfo.resize(shapes.size());
	int shapeIndex = 0;

	for (auto& shape : shapes)
	{
		//generate VAO and VBO. bind the VAO
		glGenVertexArrays(1, &glInfo[shapeIndex].m_VAO);
		glGenBuffers(1, &glInfo[shapeIndex].m_VBO);
		glBindVertexArray(glInfo[shapeIndex].m_VAO);
		glInfo[shapeIndex].m_faceCount = shape.mesh.num_face_vertices.size();
		//get the verticies
		std::vector<OBJVertex> vertices;
		int index = 0;
		for (auto face : shape.mesh.num_face_vertices)
		{
			for (int i = 0; i < 3; ++i)
			{
				tinyobj::index_t idx = shape.mesh.indices[index + 1];
				//verticies
				OBJVertex v = { 0 };
				//positions
				v.x = attribs.vertices[3 * idx.vertex_index + 0];
				v.y = attribs.vertices[3 * idx.vertex_index + 1];
				v.z = attribs.vertices[3 * idx.vertex_index + 2];
				v.w = 1;
				//normals
				if (attribs.normals.size() > 0)
				{
					v.nx = attribs.normals[3 * idx.normal_index + 0];
					v.ny = attribs.normals[3 * idx.normal_index + 1];
					v.nz = attribs.normals[3 * idx.normal_index + 2];
					v.nw = 1;
				}
				//texture coords
				if (attribs.texcoords.size() > 0)
				{
					v.u = attribs.texcoords[2 * idx.texcoord_index + 0];
					v.v = attribs.texcoords[2 * idx.texcoord_index + 1];
				}
				vertices.push_back(v);
			}
			index += face;
		}
		//create and bind VBO
		//bind the VBO
		glBindBuffer(GL_ARRAY_BUFFER, glInfo[shapeIndex].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(OBJVertex) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //position of verts
		glEnableVertexAttribArray(1); //normals
		glEnableVertexAttribArray(2); //texture data

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0); //verts
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(OBJVertex), (void*)12); //norms
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)24); //textcoords

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		shapeIndex++;
	}
}

//draw calls
void Mesh3D::Draw(Camera aCamera, int programID)
{
	if (m_meshType == OBJ) {
		for (auto& gl : glInfo)
		{
			glBindVertexArray(gl.m_VAO);
			glDrawElements(GL_TRIANGLES, gl.m_faceCount, GL_UNSIGNED_INT, 0);
			//glDrawArrays(GL_TRIANGLES, 1, gl.m_faceCount * 3);
		}
	}
}

void Mesh3D::Update(float deltatime)
{
	//currently does nothing ¯\_(ツ)_/¯
}
