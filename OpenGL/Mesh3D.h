#pragma once
#include <vector>
#include "tiny_obj_loader.h"
#include "glm\ext.hpp"
#include <vector>
#include "GlobalStructs.h"
class Camera;

enum meshType
{
	OBJ,
	UserDefined,
};
class Mesh3D
{
public:
	Mesh3D(Vertex* verticies, unsigned int numVerticies);
	Mesh3D(Vertex vertexData[], unsigned int indexData[], const char* diffuseTex, const char* normalMap = nullptr, const char* displacement = nullptr, const char* specular = nullptr);
	Mesh3D(const char* objLocation, const char* objName);
	~Mesh3D();

	void CreateOpenGLBuffers(tinyobj::attrib_t & attribs, std::vector<tinyobj::shape_t>& shapes);
	void Draw(Camera aCamera, int programID);
	void Update(float deltatime);
private:
	meshType m_meshType;
	OpenGLInfo m_glInfo;
	std::vector<OpenGLInfo> glInfo;
	//might've been easier to derive 
	unsigned int m_texture;
	unsigned int m_normal;
	unsigned int m_displacement;
	unsigned int m_specular;

	unsigned int m_IBO;
	unsigned int m_drawCount;
};

