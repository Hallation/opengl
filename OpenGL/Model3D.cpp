#include "Model3D.h"
#include "Application.h"
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#include "stb\stb_image.h"
#include "Texture.h"
#include "FrustrumCulling.h"
#include "BoundingSphere.h"
#include "AIE\Gizmos.h"
#include <iostream>
#include <string>
#if !defined TINYOBJLOADER_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#endif // !TINYOBJLOADER_IMPLEMENTATION

Model3D::Model3D()
{
	mfMovementSpeed = 10.0f;
}

Model3D::Model3D(Model3D * aModel3D)
{
	mfMovementSpeed = 10.0f;
	this->mBoundingSphere = aModel3D->mBoundingSphere;
	this->mCulling = aModel3D->mCulling;
	this->m_texture = Texture(aModel3D->m_texture);
	this->mGameObject = aModel3D->mGameObject;
	this->mOpenGlInfo.resize(aModel3D->mOpenGlInfo.size());
	for (int i = 0; i < mOpenGlInfo.size(); ++i)
	{
		this->mOpenGlInfo[i].m_faceCount = mOpenGlInfo[i].m_faceCount;
		this->mOpenGlInfo[i].m_VAO = mOpenGlInfo[i].m_VAO;
		this->mOpenGlInfo[i].m_VBO = mOpenGlInfo[i].m_VBO;
	}
}

//constructor with default position of 0, .5f, 0, 0
Model3D::Model3D(const char * objLocation, const char * objName)
{
	//Gizmos::create(10000, 10000, 10000, 10000);
	mGameObject.mvPosition = glm::vec4(0, 0.5f, 0, 0);
	std::string obj(objLocation);
	obj += objName;
	bool success = tinyobj::LoadObj(&m_Attribs, &mShapes, &mMaterials, &msErr, obj.c_str(), objLocation);
	std::cout << msErr;
	CreateOpenGlbuffers(m_Attribs, mShapes);
	//generate a shadow map
	//gen shadow map buffer
	//glGenFramebuffers(1, &mFBO);
	//glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	//
	//glGenTextures(1, &mFBODepth);
	//glBindTexture(GL_TEXTURE_2D, mFBODepth);
	//
	////a 16bit depth component
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	////attach as a depth attachment to capture depth
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mFBODepth, 0);
	////no colour targets
	//glDrawBuffer(GL_NONE);
	//
	//GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	//if (status != GL_FRAMEBUFFER_COMPLETE)
	//	std::cout << "Framebuffer error!" << std::endl;
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	testTexture = TextureLoader::LoadTexture("../bin/data/models/hand/hand.png");
}


Model3D::~Model3D()
{

}


void Model3D::Initialize(const char * objLocation, const char * objName)
{
	//Gizmos::create(10000, 10000, 10000, 10000);
	std::string obj(objLocation);
	obj += objName;
	bool success = tinyobj::LoadObj(&m_Attribs, &mShapes, &mMaterials, &msErr, obj.c_str(), objLocation);
	std::cout << msErr;
	CreateOpenGlbuffers(m_Attribs, mShapes);
	//generate a shadow map
	//gen shadow map buffer
	//glGenFramebuffers(1, &mFBO);
	//glBindFramebuffer(GL_FRAMEBUFFER, mFBO);
	//
	//glGenTextures(1, &mFBODepth);
	//glBindTexture(GL_TEXTURE_2D, mFBODepth);
	//
	////a 16bit depth component
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	////attach as a depth attachment to capture depth
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, mFBODepth, 0);
	////no colour targets
	//glDrawBuffer(GL_NONE);
	//
	//GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	//if (status != GL_FRAMEBUFFER_COMPLETE)
	//	std::cout << "Framebuffer error!" << std::endl;
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	testTexture = TextureLoader::LoadTexture("../bin/data/models/hand/hand.png");
}

void Model3D::CreateOpenGlbuffers(tinyobj::attrib_t & a_Attribs, std::vector<tinyobj::shape_t>& a_shapes)
{
	mOpenGlInfo.resize(a_shapes.size());
	int shapeIndex = 0;
	for (auto& shape : mShapes)
	{
		glGenVertexArrays(1, &mOpenGlInfo[shapeIndex].m_VAO);
		glGenBuffers(1, &mOpenGlInfo[shapeIndex].m_VBO);
		glBindVertexArray(mOpenGlInfo[shapeIndex].m_VAO);
		mOpenGlInfo[shapeIndex].m_faceCount = shape.mesh.num_face_vertices.size();
		std::vector<OBJVertex> m_Vertices;
		int index = 0;
		for (auto face : shape.mesh.num_face_vertices)
		{
			for (GLint i = 0; i < 3; ++i)
			{
				tinyobj::index_t idx = shape.mesh.indices[index + i];
				OBJVertex Vertex = { 0 };
				//vertices
				Vertex.x = a_Attribs.vertices[3 * idx.vertex_index + 0] + mGameObject.mvPosition.x;
				Vertex.y = a_Attribs.vertices[3 * idx.vertex_index + 1] + mGameObject.mvPosition.y;
				Vertex.z = a_Attribs.vertices[3 * idx.vertex_index + 2] + mGameObject.mvPosition.z;
				Vertex.w = 1;
				//normals
				if (a_Attribs.normals.size() > 0)
				{
					Vertex.nx = a_Attribs.normals[3 * idx.normal_index + 0];
					Vertex.ny = a_Attribs.normals[3 * idx.normal_index + 1];
					Vertex.nz = a_Attribs.normals[3 * idx.normal_index + 2];
					Vertex.nw = 1;
				}
				//texture coords
				if (a_Attribs.texcoords.size() > 0)
				{
					Vertex.u = a_Attribs.texcoords[2 * idx.texcoord_index + 0];
					Vertex.v = 1 - a_Attribs.texcoords[2 * idx.texcoord_index + 1];
				}
				m_Vertices.push_back(Vertex);
			}
			index += face;
		}
		glBindBuffer(GL_ARRAY_BUFFER, mOpenGlInfo[shapeIndex].m_VBO);
		glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(OBJVertex), m_Vertices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); //position of verts
		glEnableVertexAttribArray(1); //normals
		glEnableVertexAttribArray(2); //texture data

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), 0); //verts
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(OBJVertex), (void*)16); //norms
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(OBJVertex), (void*)32); //textcoords

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		shapeIndex++;
	}
}

void Model3D::Draw(glm::mat4 ProjectionView, glm::mat4 CameraWorld, glm::vec4 lightColour, glm::vec4 lightDir)
{
	//if (mCulling.SphereCullCheck(ProjectionView, mBoundingSphere.GetCentre(), mBoundingSphere.GetRadius())) {
		for (auto& gl : mOpenGlInfo)
		{
			//Gizmos::addSphere(mBoundingSphere.GetCentre(), mBoundingSphere.GetRadius(), 50, 50, glm::vec4(0));
			float SpecPow = 500;
			m_texture.DrawTexture();
			int loc = glGetUniformLocation(m_texture.GetProgramID(), "projectionView");
			glUniformMatrix4fv(loc, 1, GL_FALSE, &ProjectionView[0][0]);
			//vec3 light(sin(glfwGetTime()), 1, cos(glfwGetTime()));
			loc = glGetUniformLocation(m_texture.GetProgramID(), "lightDir");
			glUniform3f(loc, lightDir.x, lightDir.y, lightDir.z);
			//uniform lightdir camerapos specpow lightcolour diffuse normal
			loc = glGetUniformLocation(m_texture.GetProgramID(), "lightColour");
			glUniform4f(loc, lightColour.x, lightColour.y, lightColour.z, lightColour.w);
			loc = glGetUniformLocation(m_texture.GetProgramID(), "mPosition");
			glUniform4f(loc, mGameObject.mvPosition.x, mGameObject.mvPosition.y, mGameObject.mvPosition.z, mGameObject.mvPosition.w);
			glBindVertexArray(gl.m_VAO);
			glDrawArrays(GL_TRIANGLES, 0, gl.m_faceCount * 3);
		}
	//}
}

void Model3D::Update(float deltatime)
{

}

void Model3D::DrawVerts(glm::mat4 projectionView)
{
	for (auto& gl : mOpenGlInfo)
	{
		glBindVertexArray(gl.m_VAO);
		glDrawArrays(GL_TRIANGLES, 0, gl.m_faceCount * 3);
	}
}

void Model3D::LoadTexture(char * a_diffuse, char * a_normal, char * a_specular)
{
	m_texture.LoadTextures(a_diffuse, a_normal, a_specular);
}

void Model3D::LoadShaders(const char * vertShader, const char * fragShader)
{
	m_texture.LoadShaders(vertShader, fragShader);
}

void Model3D::SetPosition(glm::vec4 avPosition)
{
	mGameObject.mvPosition = avPosition;
}

void Model3D::AddPosition(glm::vec4 avPosition)
{
	mGameObject.mvPosition += avPosition;
}

void Model3D::SetSphereOffset(glm::vec3 a_vSphereOffset)
{
	
	mBoundingSphere.SetCentre(glm::vec3(mGameObject.mvPosition)+ a_vSphereOffset);
}

void Model3D::SetSphereRadius(float a_fSphereRadius)
{
	mBoundingSphere.SetRadius(a_fSphereRadius);
}
