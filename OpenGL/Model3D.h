#pragma once
#include <vector>
#include "tiny_obj_loader.h"
#include "glm\ext.hpp"
#include "IncludeHelpers.h"
#include "structs.h"
#include "BoundingSphere.h"
#include "FrustrumCulling.h"
#include "Texture.h"
class Model3D
{
public:
	Model3D();
	Model3D(Model3D* aModel3D); //copy construcotr
	Model3D(const char* objLocation, const char* objName);
	~Model3D();

	void Initialize(const char* objLocation, const char* objName);
	void CreateOpenGlbuffers(tinyobj::attrib_t &a_Attribs, std::vector<tinyobj::shape_t> & a_shapes);
	
	//draw every frame
	virtual void Draw(glm::mat4 ProjectionView, glm::mat4 CameraWorld, glm::vec4 lightColour, glm::vec4 lightDir);
	virtual void Update(float deltatime);

	void DrawVerts(glm::mat4 projectionView);
	//load my texture and shader program
	void LoadTexture(char* a_diffuse, char* a_normal = nullptr, char* a_specular = nullptr);
	void LoadShaders(const char* vertShader, const char* fragShader);
	void SetPosition(glm::vec4 avPosition);
	void AddPosition(glm::vec4 avPosition);
	glm::vec4* GetPosition()
	{
		return &mGameObject.mvPosition;
	}
	float* MovementSpeed()
	{
		return &mfMovementSpeed;
	}
	float GetSphereRadius()
	{
		return mBoundingSphere.GetRadius();
	}
	glm::vec3 GetSphereOffset()
	{
		return m_vSphereOffset;
	}
	void SetSphereOffset(glm::vec3 a_vSphereOffset);
	void SetSphereRadius(float a_fSphereRadius);

	GameObject GetGameObject()
	{
		return mGameObject;
	}
private:

	int testTexture;
	std::string msErr;
	tinyobj::attrib_t m_Attribs;
	std::vector<tinyobj::shape_t> mShapes;
	std::vector<tinyobj::material_t> mMaterials;


protected:
	glm::vec3 m_vSphereOffset;
	unsigned int m_diffuse;
	unsigned int m_normal;
	unsigned int m_specular;
	float mfMovementSpeed;
	GameObject mGameObject;
	std::vector<OpenGLInfo> mOpenGlInfo;
	FrustrumCulling mCulling;
	BoundingSphere mBoundingSphere;
	Texture m_texture;
};

