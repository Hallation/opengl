#include "Moon.h"
#include "AIE\Gizmos.h"

Moon::Moon()
{
	mLocalTransform = glm::mat4(1);
}

Moon::Moon(glm::vec4 aOffset, float afRadius, int aiRow, int aiColumn, const float aRotationSpeed, const glm::vec4 fillColour) : 
	Planet(aOffset, afRadius, aiRow, aiColumn, aRotationSpeed, fillColour)
{
	mLocalTransform = glm::mat4(1);
}


Moon::~Moon()
{
}

void Moon::UpdateParent(glm::mat4 aParentTransform)
{
	mLocalTransform = aParentTransform * mMatrix;
}

void Moon::Update(float deltatime)
{
	mRotation = glm::rotate(fRotationSpeed * deltatime, glm::vec3(1, 0, 0));
	mLocalTransform = mLocalTransform * mDisplacement *  mRotation;
}

void Moon::Draw()
{
	Gizmos::addSphere(glm::vec3(mLocalTransform[3]), fRadius, iRows, iColumns, fillColour, &mLocalTransform);
}

glm::mat4 Moon::GetMatrix()
{
	return mLocalTransform * mMatrix;
}
