#pragma once
#include "Planet.h"
class Moon :
	public Planet
{
	using Planet::Planet;
public:
	Moon();
	Moon(glm::vec4 aOffset, float afRadius, int aiRow, int aiColumn, const float aRotationSpeed, const glm::vec4 fillColour);
	~Moon();

	void UpdateParent(glm::mat4 aParentTransform);
	void Update(float deltatime);
	void Draw();

	glm::mat4 GetMatrix();
private:
	glm::mat4 mLocalTransform;
};

