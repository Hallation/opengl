#pragma once
#include "glm\ext.hpp"

struct Particle
{
public:
	//particles have a position, velocity, size and colour;
	glm::vec3 mPosition;
	glm::vec3 velocity;
	glm::vec4 mColour;
	float size;
	float lifetime;
	float lifespan;
	
};

