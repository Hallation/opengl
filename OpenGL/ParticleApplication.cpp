#include "ParticleApplication.h"
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#include "FPSCamera.h"
#include "IncludeHelpers.h"
#include "ParticleEmitter.h"
#include "Grid.h"
ParticleApplication::ParticleApplication()
{
}


ParticleApplication::~ParticleApplication()
{
}

bool ParticleApplication::Startup()
{
	Application::Initialize();
	mGrid = new Grid(64, 64);
	//define my camera
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);

	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	m_programID = (ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/particleVS.glsl", "../bin/data/shaders/fragShaders/particleFS.glsl"));
	//still gotta fix particle emitter
	m_emitter = new ParticleEmitter();
	//max particles, emit rate, min lifetime, max lifetime, min velocity, max velocity, start size, end size, start colour, end colour
	m_emitter->Initialise(1000, 500,
		0.1f, 1.0f,
		1, 5,
		1, 0.1f,
		glm::vec4(1, 0, 0, 1), glm::vec4(1, 1, 0, 1));
	m_emitter->InitialiseShader();
	//m_emitter->BindCamera(mCamera->GetProjectionView());
	return true;
}

bool ParticleApplication::Shutdown()
{
	delete mGrid;
	delete m_emitter;
	delete mCamera;
	Application::DestroyWindow();
	return false;
}

void ParticleApplication::Run()
{
	Startup();
	glfwMakeContextCurrent(mWindow);
	//glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	while (glfwWindowShouldClose(mWindow) == false && glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		currTime = (float)glfwGetTime();
		deltaTime = currTime - prevTime;
		prevTime = currTime; //deltatime

		Update(deltaTime);
		Draw();

		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}
}

void ParticleApplication::Draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//int loc = glGetUniformLocation(m_programID, "projectionView");
	//glUniformMatrix4fv(loc, 1, GL_FALSE, &mCamera->GetProjectionView()[0][0]);
	//
	m_emitter->BindCamera(mCamera->GetProjectionView());
	m_emitter->Draw();
	//mGrid->Draw();
}

void ParticleApplication::Update(float deltatime)
{
	m_emitter->Update(deltatime, mCamera->GetWorldTransform());
	mCamera->Update(mWindow, deltatime);
}