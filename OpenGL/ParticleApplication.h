#pragma once
#include "Application.h"
class ParticleEmitter;
class Grid;
class ParticleApplication :
	public Application
{
public:
	ParticleApplication();
	~ParticleApplication();

	bool Startup();
	bool Shutdown();

	void Run();
	void Draw();
	void Update(float deltatime);

private:
	ParticleEmitter* m_emitter;
	Grid* mGrid;
};

