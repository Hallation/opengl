#include "ParticleEmitter.h"
#include "gl_core_4_4.h"
#include "Particle.h"
#include "IncludeHelpers.h"
#include <iostream>
ParticleEmitter::ParticleEmitter()
	:m_particles(nullptr),
	m_firstDead(0),
	m_maxParticles(0),
	m_vPosition(0, 0, 0),
	m_vao(0), m_vbo(0), m_ibo(0),
	m_vertexData(nullptr){
}


ParticleEmitter::~ParticleEmitter()
{
	delete[] m_particles;
	delete[] m_vertexData;

	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
}

void ParticleEmitter::Initialise(unsigned int a_maxParticles, unsigned int a_emitRate, float a_lifetimeMin, float a_lifetimeMax, float a_velocityMin, float a_velocityMax, float a_startSize, float a_endSize, const glm::vec4 & a_startColour, const glm::vec4 & a_endColour)
{
	m_emitTimer = 0;
	m_emitRate = 1.0f / a_emitRate;

	m_startColour = a_startColour;
	m_endColour = a_endColour;
	m_startSize = a_startSize;
	m_endSize = a_endSize;
	m_velocityMin = a_velocityMin;
	m_velocityMax = a_velocityMax;
	m_lifespanMin = a_lifetimeMin;
	m_lifespanMax = a_lifetimeMax;
	m_maxParticles = a_maxParticles;

	//make particle array
	m_particles = new Particle[m_maxParticles];
	m_firstDead = 0;

	//create array of verts for particles
	//filled during the update
	m_vertexData = new ParticleVertex[m_maxParticles * 4];

	//create index buffer for particles
	//6 indices per quad
	unsigned int* indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; ++i)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	//create opengl buffers
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	//vertex data gets filled in update
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(ParticleVertex), m_vertexData, GL_DYNAMIC_DRAW);

	//bind the ibo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //position
	glEnableVertexAttribArray(1); //colour

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indexData;
}

void ParticleEmitter::InitialiseShader()
{
	m_shader = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/particleVS.glsl", "../bin/data/shaders/fragShaders/particleFS.glsl");
}

void ParticleEmitter::Update(float deltatime, const glm::mat4 & a_cameraTransform)
{
	//updating the particles
	//update position, velocity and elapsed delta time
	//update size. interpolate between start and end size. using lifetime as interpolation value
	//update its colour
	//interpolate between start and end
	//use lifetime as time between

	m_emitTimer += deltatime;

	while (m_emitTimer > m_emitRate)
	{
		Emit();
		m_emitTimer -= m_emitRate;
	}
	unsigned int quad = 0;
	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particle* particle = &m_particles[i];
		particle->lifetime += deltatime;

		if (particle->lifetime >= particle->lifespan)
		{
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;
		}
		else
		{
			//move the particle
			particle->mPosition += particle->velocity * deltatime;

			//size the particle
			particle->size = glm::mix(m_startSize, m_endSize, particle->lifetime / particle->lifespan);

			//colour the particle
			particle->mColour = glm::mix(m_startColour, m_endColour, particle->lifetime / particle->lifespan);

			//make quad the correct size
			float halfSize = particle->size * 0.5f;
			m_vertexData[quad * 4 + 0].position = glm::vec4(halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].colour = particle->mColour;

			m_vertexData[quad * 4 + 1].position = glm::vec4(-halfSize, halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->mColour;

			m_vertexData[quad * 4 + 2].position = glm::vec4(-halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].colour = particle->mColour;

			m_vertexData[quad * 4 + 3].position = glm::vec4(halfSize, -halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->mColour;

			// create the billboard
			glm::vec3 zAxis = glm::normalize(glm::vec3(a_cameraTransform[3]) - particle->mPosition);	//z axis is normalized vector from particle's position to the camera pos
			glm::vec3 xAxis = glm::cross(glm::vec3(a_cameraTransform[1]), zAxis); //xaxis is cross product between camera Up and the billboard z axis
			glm::vec3 yAxis = glm::cross(zAxis, xAxis); //y axis is cross between z and x
			glm::mat4 billboard(
				glm::vec4(xAxis, 0),
				glm::vec4(yAxis, 0),
				glm::vec4(zAxis, 0),
				glm::vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard * m_vertexData[quad * 4 + 0].position + glm::vec4(particle->mPosition, 0);
			m_vertexData[quad * 4 + 1].position = billboard * m_vertexData[quad * 4 + 1].position + glm::vec4(particle->mPosition, 0);
			m_vertexData[quad * 4 + 2].position = billboard * m_vertexData[quad * 4 + 2].position + glm::vec4(particle->mPosition, 0);
			m_vertexData[quad * 4 + 3].position = billboard * m_vertexData[quad * 4 + 3].position + glm::vec4(particle->mPosition, 0);

			++quad;
		}
	}
}

void ParticleEmitter::Draw()
{
	//sync vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(ParticleVertex), m_vertexData);

	//draw the particles
	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);
}

void ParticleEmitter::BindCamera(glm::mat4 projectionView)
{
	glUseProgram(m_shader);
	int loc = glGetUniformLocation(m_shader, "projectionView");
	assert(loc != -1 && "Error: Failed to bind projection view");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &projectionView[0][0]);
}

void ParticleEmitter::SetPosition(glm::vec3 avPosition)
{
	m_vPosition = avPosition;
}

void ParticleEmitter::SetPosition(float x, float y, float z)
{
	m_vPosition = glm::vec3(x, y, z);
}

void ParticleEmitter::Emit()
{
	//only emite if there is a dead particle
	if (m_firstDead >= m_maxParticles)
		return;

	// resurrect first dead particle
	Particle& particle = m_particles[m_firstDead++];

	// assign starting pos
	particle.mPosition = m_vPosition;

	//randomise its lifetime
	particle.lifetime = 0;
	particle.lifespan = (rand() / (float)RAND_MAX) * (m_lifespanMax - m_lifespanMin) + m_lifespanMin;

	//set starting size and colour
	particle.mColour = m_startColour;
	particle.size = m_startSize;

	//randomise velocity dir and strength
	float velocity = (rand() / (float)RAND_MAX) * (m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity = glm::normalize(particle.velocity) * velocity;
}
