#pragma once
#include "glm\ext.hpp"
struct Particle;

struct ParticleVertex
{
	glm::vec4 position;
	glm::vec4 colour;
};
class ParticleEmitter
{
public:
	ParticleEmitter();
	virtual ~ParticleEmitter();

	void Initialise(unsigned int a_maxParticles, unsigned int a_emitRate,
		float a_lifetimeMin, float a_lifetimeMax,
		float a_velocityMin, float a_velocityMax,
		float a_startSize, float a_endSize,
		const glm::vec4& a_startColour, const glm::vec4& a_endColour);
	void InitialiseShader();
	void Emit();
	void Update(float deltatime, const glm::mat4& a_cameraTransform);
	void Draw();
	void BindCamera(glm::mat4 projectionView);
	void SetPosition(glm::vec3 avPosition);
	void SetPosition(float x, float y, float z);
protected:
	Particle* m_particles;
	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	//opengl buffers
	unsigned int m_vao, m_vbo, m_ibo;
	//position of the emitter
	glm::vec3 m_vPosition;
	//emit rate
	float m_emitTimer;
	float m_emitRate;

	//life span of the particles
	float m_lifespanMin;
	float m_lifespanMax;

	//min and max velocities
	float m_velocityMin;
	float m_velocityMax;

	//size of the particles
	float m_startSize;
	float m_endSize;

	//shader program
	unsigned int m_shader;

	glm::vec4 m_startColour;
	glm::vec4 m_endColour;
	ParticleVertex* m_vertexData;
};

