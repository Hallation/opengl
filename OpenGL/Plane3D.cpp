#include "Plane3D.h"
#include "gl_core_4_4.h"
#include <iostream>
#if !defined STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb\stb_image.h"
#endif // !STB_IMAGE_IMPLMENTATION



Plane3D::Plane3D()
{
}

Plane3D::Plane3D(Vertex vertexData[], unsigned int indexData[], const char * diffuseTex, const char * normalMap, const char * displacement, const char * specular)
{
	mNumberOfIndices = 6;
	int imageWidth;
	int imageHeight;
	int imageFormat;
	unsigned char* data;

	if (diffuseTex != nullptr) {
		data = stbi_load("../bin/data/textures/four/four_diffse.tga", &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_texture); //generate our texture
		glBindTexture(GL_TEXTURE_2D, m_texture); //make sure it goes to the right position
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data); //load the texture data
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data);
	}
	//sanity checks
	if (normalMap != nullptr) {
		data = stbi_load(normalMap, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_normal);
		glBindTexture(GL_TEXTURE_2D, m_normal);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (displacement != nullptr) {
		data = stbi_load(displacement, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_displacement);
		glBindTexture(GL_TEXTURE_2D, m_displacement);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (specular != nullptr) {
		data = stbi_load(specular, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_specular);
		glBindTexture(GL_TEXTURE_2D, m_specular);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}

	//genereate VAO;
	glGenVertexArrays(1, &m_glInfo.m_VAO);
	glBindVertexArray(m_glInfo.m_VAO);

	//create and bind VBO
	glGenBuffers(1, &m_glInfo.m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_glInfo.m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glEnableVertexAttribArray(1); //norms
	glEnableVertexAttribArray(2); //texcoords
	glEnableVertexAttribArray(3); //tangents
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); //position
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 16); //norms
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 48); //tex coords
	//glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 32); //tangents
																		
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
Plane3D::Plane3D(const char * diffuseTex, const char * normalMap, const char * displacement, const char * specular)
{

	Vertex vertexData[] =
	{
		{ -10,0, 10,1,0,1,0,0,1,0,0,0,0,1 },
		{ 10,0, 10,1,0,1,0,0,1,0,0,0,1,1 },
		{ 10,0,-10,1,0,1,0,0,1,0,0,0,1,0 },
		{ -10,0,-10,1,0,1,0,0,1,0,0,0,0,0 },
	};
	//index data
	unsigned int indexData[] =
	{
		0,1,2,
		0,2,3,
	};

	mNumberOfIndices = 6;
	int imageWidth;
	int imageHeight;
	int imageFormat;
	unsigned char* data;

	if (diffuseTex != nullptr) {
		data = stbi_load("../bin/data/textures/four/four_diffse.tga", &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_texture); //generate our texture
		glBindTexture(GL_TEXTURE_2D, m_texture); //make sure it goes to the right position
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data); //load the texture data
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data);
	}
	//sanity checks
	if (normalMap != nullptr) {
		data = stbi_load(normalMap, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_normal);
		glBindTexture(GL_TEXTURE_2D, m_normal);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (displacement != nullptr) {
		data = stbi_load(displacement, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_displacement);
		glBindTexture(GL_TEXTURE_2D, m_displacement);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}
	if (specular != nullptr) {
		data = stbi_load(specular, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, &m_specular);
		glBindTexture(GL_TEXTURE_2D, m_specular);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //Texture filtering
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //Texture filtering
		stbi_image_free(data); //free the data now that the texture is loaded
							   //x y z w, U,V
	}

	//genereate VAO;
	glGenVertexArrays(1, &m_glInfo.m_VAO);
	glBindVertexArray(m_glInfo.m_VAO);

	//create and bind VBO
	glGenBuffers(1, &m_glInfo.m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_glInfo.m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, vertexData, GL_STATIC_DRAW);

	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 6, indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glEnableVertexAttribArray(1); //norms
	glEnableVertexAttribArray(2); //texcoords
	glEnableVertexAttribArray(3); //tangents
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); //position
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 16); //norms
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 48); //tex coords
																					  //glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + 32); //tangents

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
Plane3D::~Plane3D()
{
}

void Plane3D::Draw()
{
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, m_texture);
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, m_normal);

	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, mFBODepth);

	glBindVertexArray(m_glInfo.m_VAO);
	glDrawElements(GL_TRIANGLES, mNumberOfIndices, GL_UNSIGNED_INT, nullptr);
}

void Plane3D::Generate(unsigned int rows, unsigned int cols)
{
	GridVerts* aoVerticies = new GridVerts[rows * cols];
	//genereate verticies for our grid
	mNumberOfIndicies = rows - 1 * cols - 1 * 6;
	for (unsigned int r = 0; r < rows; ++r)
	{
		for (unsigned int c = 0; c < cols; c++)
		{
			aoVerticies[r* cols + c].position = glm::vec4((float)c, 0, (float)r, 1);

			//create some arbitrary colour
			glm::vec3 colour = glm::vec3(sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
			aoVerticies[r * cols + c].colour = glm::vec4(colour, 1);
		}
	}

	//calculate indicies
	unsigned int* auiIndicies = new unsigned int[(rows - 1) * (cols - 1) * 6];
	unsigned int index = 0;
	for (unsigned int r = 0; r < (rows - 1); ++r)
	{
		for (unsigned int c = 0; c < (cols - 1); ++c)
		{
			//first triangle
			auiIndicies[index++] = r * cols + c;
			auiIndicies[index++] = (r + 1) * cols + c;
			auiIndicies[index++] = (r + 1) * cols + (c + 1);

			//second triangle
			auiIndicies[index++] = r * cols + c;
			auiIndicies[index++] = (r + 1) * cols + (c + 1);
			auiIndicies[index++] = r *cols * (c + 1);
		}
	}
	glGenBuffers(1, &m_glInfo.m_VBO);
	glGenBuffers(1, &m_IBO);

	//genereate VAO;
	glGenVertexArrays(1, &m_glInfo.m_VAO);
	glBindVertexArray(m_glInfo.m_VAO);
	//create and bind VBO
	glBindBuffer(GL_ARRAY_BUFFER, m_glInfo.m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(GridVerts), aoVerticies, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //verts
	glEnableVertexAttribArray(1); //norms 
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(GridVerts), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(GridVerts), (void*)(sizeof(glm::vec4)));

	//create and bind the IBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndicies, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] aoVerticies;
	delete[] auiIndicies;
}

void Plane3D::GenerateNoise()
{
	//int dims = 64;
	//float* perlinData = new float[dims * dims];
	////scale our points down
	//int octaves = 6;
	//float scale = (1.0f / dims) * 3;
	////use glm perlin function to create noise
	//for (int x = 0; x < 64; ++x)
	//{
	//	for (int y = 0; y < 64; ++y)
	//	{
	//		float amplitude = 1.f;
	//		float persistence = 0.3f;
	//		perlinData[y * dims + x] = 0;
	//		for (int o = 0; o < octaves; ++o)
	//		{
	//			float freq = powf(2, (float)o);
	//			float perlinSample = glm::perlin(glm::vec2((float)x, (float)y * scale * freq) * 0.5f + 0.5f);
	//			perlinData[y * dims + x] += perlinSample * amplitude;
	//			amplitude += persistence;
	//		}
	//		//generate perlin noise
	//	}
	//}

	//make a texture containing the perlin noise
	//glGenTextures(1, &m_texture);
	//glBindTexture(GL_TEXTURE_2D, m_texture);
	//
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlinData);
	//
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//
	//
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//delete[] perlinData;
}