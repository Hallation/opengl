#pragma once
#include "Model3D.h"

struct GridVerts
{
	glm::vec4 position;
	glm::vec4 colour;
};
class Plane3D : public Model3D 
{
public:
	Plane3D();
	Plane3D(Vertex vertexData[], unsigned int indexData[], const char * diffuseTex = nullptr, const char * normalMap = nullptr, const char * displacement = nullptr, const char * specular = nullptr);
	Plane3D(const char* diffuseTex = nullptr, const char* NormalMap = nullptr, const char* displacement = nullptr, const char* specular = nullptr);
	~Plane3D();

	void GenerateNoise();
	void Draw();
	void Generate(unsigned int aXSize, unsigned int aYSize);
private:
	unsigned int m_texture;
	unsigned int m_normal;
	unsigned int m_displacement;
	unsigned int m_specular;

	unsigned int m_IBO;

	unsigned int mFBO;
	unsigned int mFBODepth;
	
	int mNumberOfIndices;
	OpenGLInfo m_glInfo;
	int mNumberOfIndicies;
};

