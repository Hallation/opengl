#include "Planet.h"
#include "AIE\Gizmos.h"
#include <iostream>

#define ROTATION_SPEED 1.f;
#define ROTATION_INCREMENT 0.2f;
Planet::Planet()
{
	mMatrix = glm::mat4(1);
	mRotation = glm::mat4(1);
	mDisplacement[3] = glm::vec4(0);
	fRadius = 0.8f;
	iRows = 10;
	iColumns = 10;
	fRotationIncrement = ROTATION_INCREMENT;
	fRotationSpeed = ROTATION_SPEED;
}

Planet::Planet(glm::vec4 aOffset, float afRadius, int aiRow, int aiColumn, const float aRotationSpeed, const glm::vec4 afillColour)
{
	mMatrix = glm::mat4(1);
	mRotation = glm::mat4(1);
	mDisplacement[3] = aOffset;
	fRadius = afRadius;
	iRows = aiRow;
	iColumns = aiColumn;
	fRotationSpeed = aRotationSpeed;
	fillColour = afillColour;
	fRotationIncrement = ROTATION_INCREMENT;
}

Planet::Planet(glm::vec4 avPosition)
{
	mMatrix = glm::mat4(1);
	mRotation = glm::mat4(1);
	mDisplacement[3] = avPosition;
	fRotationSpeed = ROTATION_SPEED;
	fRadius = 0.5f;
	iRows = 10;
	iColumns = 10;
	fRotationIncrement = ROTATION_INCREMENT;
}


Planet::~Planet()
{
}

void Planet::Update(float deltatime)
{
	//mMatrix[3] += vec4(0.2f * deltatime,0,0,0);
	//rotate around the planet
	mRotation = glm::rotate(fRotationSpeed * deltatime, glm::vec3(0, 1, 0));
	mMatrix = mMatrix * mDisplacement * mRotation;
}

void Planet::Draw()
{
	//Gizmos::addSphere(vec3(mLocalMatrix[3]), 0.6f, 20, 20, vec4(0), &mLocalMatrix);
	Gizmos::addSphere(glm::vec3(mMatrix[3]), fRadius, iRows, iColumns, fillColour, &mMatrix);
}

glm::mat4 Planet::GetMatrix()
{
	return mMatrix;
}
