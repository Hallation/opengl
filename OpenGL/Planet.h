#pragma once
#include <glm\glm.hpp>
#include <glm\ext.hpp>

class Planet
{
public:
	Planet();
	Planet(glm::vec4 aOffset, float afRadius, int aiRow, int aiColumn, const float aRotationSpeed, const glm::vec4 fillColour);
	Planet(glm::vec4 avPosition);
	~Planet();

	virtual void Update(float deltatime);
	virtual void Draw();

	virtual glm::mat4 GetMatrix();

protected:
	glm::mat4 mDisplacement;
	glm::mat4 mMatrix;
	glm::mat4 mRotation;
	glm::vec4 fillColour;
	float fRotationSpeed;
	float fRadius;
	float fRotationIncrement;
	int iRows;
	int iColumns;
};

