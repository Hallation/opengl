#include "ProceduralApplication.h"
#include "gl_core_4_4.h"
#include "FPSCamera.h"
#include "GLFW\glfw3.h"
#include "IncludeHelpers.h"
#include "Plane3D.h"
ProceduralApplication::ProceduralApplication()
	:perlinData(nullptr)
{
	
}


ProceduralApplication::~ProceduralApplication()
{

}

bool ProceduralApplication::Shutdown()
{
	delete mCamera;
	delete m_Grid;
	delete BunnyOBJ;
	glfwDestroyWindow(mWindow);
	glfwTerminate();
	return false;
}

bool ProceduralApplication::Startup()
{
	
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera = new FPSCamera();
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);

	//initlizae glfw
	if (glfwInit() == false)
		return false;

	//define the window
	GLFWwindow* window = glfwCreateWindow(1280, 720, "graphics", nullptr, nullptr);
	mWindow = window; //set mwindow pointer

	if (window == nullptr)
	{
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(mWindow);

	//load gl function
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(mWindow);
		glfwTerminate();
		return false;
	}
	auto major = ogl_GetMajorVersion();
	auto minor = ogl_GetMinorVersion();
	printf("GL: %i.%i\n", major, minor);
	glfwGetWindowSize(mWindow, &iWidth, &iHeight);

	programID = ShaderLoader::LoadShader(
		"../bin/data/shaders/vertShaders/proceduralVS.glsl",
		"../bin/data/shaders/fragShaders/proceduralFS.glsl");
	glClearColor(0.25f, 0.25f, 0.25f, 1); //clear color
	glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	m_Grid = new Grid(64,64);
	GenerateNoise(64,64);
	BunnyOBJ = new Model3D("../bin/data/models/", "cube.obj");
	return true;
}

void ProceduralApplication::Run()
{
	Startup();
	deltaTime = 0;
	prevTime = 0;
	glfwMakeContextCurrent(mWindow);
	//glfwSetMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	while (glfwWindowShouldClose(mWindow) == false && glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		currTime = (float)glfwGetTime();
		deltaTime = currTime - prevTime;
		prevTime = currTime; //deltatime

		Update(deltaTime);
		Draw();
		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}
}

void ProceduralApplication::Draw()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(programID);
	int loc = glGetUniformLocation(programID, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &(mCamera->GetProjectionView()[0][0]));
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_perlinTexture);
	m_Grid->Draw(mCamera->GetProjectionView());
	//m_Plane->Draw();
}

void ProceduralApplication::Update(float deltatime)
{
	mCamera->Update(mWindow,deltatime);
}

void ProceduralApplication::GenerateNoise(float afWidth, float afLength)
{
	int dims = 64;
	float* perlinData = new float[dims * dims];
	//scale our points down
	int octaves = 6;
	float scale = (1.0f / dims) * 3;
	//use glm perlin function to create noise
	for (int x = 0; x < 64; ++x)
	{
		for (int y = 0; y < 64; ++y)
		{
			float amplitude = 1.f;
			float persistence = 0.3f;
			perlinData[y * dims + x] = 0;
			for (int o = 0; o < octaves; ++o)
			{
				float freq = powf(2, (float)o);
				float perlinSample = glm::perlin(glm::vec2((float)x, (float)y) * scale * freq) * 0.5f + 0.5f;
				perlinData[y * dims + x] += perlinSample * amplitude;
				amplitude *= persistence;
			}
			//perlinData[y * dims + x] = glm::perlin(vec2(x, y) * scale) * 0.5f + 0.5f;
			//generate perlin noise
		}
	}

	//make a texture containing the perlin noise
	glGenTextures(1, &m_perlinTexture);
	glBindTexture(GL_TEXTURE_2D, m_perlinTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 64, 64, 0, GL_RED, GL_FLOAT, perlinData);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//clamp the texture to the edge
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	delete[] perlinData;
}
