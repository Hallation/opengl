#pragma once
#include "Application.h"

class Plane3D;
class Grid;
class Model3D;
class ProceduralApplication :
	public Application
{
public:
	ProceduralApplication();
	~ProceduralApplication();

	bool Shutdown();
	bool Startup();

	void Run();
	void Draw();
	void Update(float deltatime);
	/*
	virtual shutdown
	virtual startup
	virtual run

	protected:
	bool wireFrame;
	FPSCamera* mCamera;
	std::vector<int> m_Programs;
	unsigned int mBasicProgram;
	unsigned int mGenerateShadow;
	unsigned int mUseShadow;
	unsigned int mParticleShaders;
	//deltatime stuff
	float prevTime;
	float currTime;
	float deltaTime;

	int iHeight;
	int iWidth;
	*/

	void GenerateNoise(float afWidth, float afLength);
private:
	float* perlinData;
	unsigned int m_perlinTexture;
	unsigned int programID;
	Grid* m_Grid;
	Plane3D* m_Plane;
	Model3D* BunnyOBJ;
};

