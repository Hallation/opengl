#include "ShaderLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

GLuint ShaderLoader::LoadShader(const char* vsPath, const char* fsPath)
{
	GLuint program;
	std::string vsSource; 
	std::string fsSource; 
	vsSource = ReadShaderFile(vsPath);
	fsSource = ReadShaderFile(fsPath);
	const char* vertShader = vsSource.c_str();
	const char* fragShader = fsSource.c_str();
	int success = GL_FALSE;
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	//get the shader source and compile thhem
	glShaderSource(vertexShader, 1, (char**)&vertShader, 0);
	glCompileShader(vertexShader);

	glShaderSource(fragmentShader, 1, (char**)&fragShader, 0);
	glCompileShader(fragmentShader);

	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(program, infoLogLength, 0, infoLog);
		printf("Error: Failed to link shade program!\n");
		printf("%s\n", infoLog);
		delete[] infoLog;
	}
	else
	{
		printf("Success: Shade program successfully linked\n");
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return program;
}

std::string ShaderLoader::ReadShaderFile(const char* filename)
{
	{
		// Create an input filestream and attempt to open the specified file
		std::ifstream file(filename);

		// if file is shit, abandon ship
		if (!file.good())
		{
			throw std::runtime_error("Failed to open file: " + *filename);
		}
		//make a stream
		std::stringstream stream;

		// literally dumping our shit into the stream
		stream << file.rdbuf();

		// close it
		file.close();

		// convert the stream into a string
		return stream.str();
	}
}
