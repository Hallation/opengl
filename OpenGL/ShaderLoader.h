#pragma once
#include <string>
#include "gl_core_4_4.h"
class ShaderLoader
{
public:

	static GLuint LoadShader(const char* vsPath, const char* fsPath);

	static std::string ReadShaderFile(const char* filename);

};

