#include "Texture.h"
#include "gl_core_4_4.h"
#include "IncludeHelpers.h"

Texture::Texture()
{
	//test data only
	//testMap = TextureLoader::LoadTexture("../bin/data/models/soulspear/soulspear_diffuse.tga");
	//programID = ShaderLoader::LoadShader("../bin/data/shaders/vertShaders/basicVS.glsl", "../bin/data/shaders/fragShaders/FragShader.glsl");
}

Texture::Texture(Texture * aTexture)
{
	this->programID = aTexture->programID;
	this->DiffuseMap = aTexture->DiffuseMap;
	this->DisplamenetMap = aTexture->DisplamenetMap;
	this->SpecularMap = aTexture->SpecularMap;
}


Texture::~Texture()
{
}

void Texture::LoadTextures(const char * diffuseDir, const char * normalDir, const char * displacementDir, const char * specularDir)
{
	DiffuseMap = TextureLoader::LoadTexture(diffuseDir);
	NormalMap = TextureLoader::LoadTexture(normalDir);
	SpecularMap = TextureLoader::LoadTexture(specularDir);
}

void Texture::LoadShaders(const char * a_vertShader, const char * a_fragShader)
{
	programID = ShaderLoader::LoadShader(a_vertShader, a_fragShader);
}

void Texture::DrawTexture()
{
	// Use texture program.
	glUseProgram(programID);

	// Set texture slot.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, DiffuseMap);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, NormalMap);

	// Tell the shader where it is.
	GLint m_iLocation = glGetUniformLocation(programID, "diffuse");
	glUniform1i(m_iLocation, 0);
	m_iLocation = glGetUniformLocation(programID, "normal");
	glUniform1i(m_iLocation, 1);
}

void Texture::Draw(glm::mat4 projectionView)
{
	glUseProgram(programID);
	int loc = glGetUniformLocation(programID, "projectionView");
	glUniformMatrix4fv(loc, 1, GL_FALSE, &projectionView[0][0]);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, testMap);
	loc = glGetUniformLocation(programID, "diffuse");
	glUniform1i(loc, 0);

}
