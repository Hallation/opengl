#pragma once
#include <string>
#include "glm\glm.hpp"
struct TextureVertex
{
	float x, y, z, w; //vert position
	float nx, ny, nz, nw; //normals
	float tx, ty, tz, tw; //tangnents
	float s, t; //texture coordinates
};
class Texture
{
public:
	Texture();
	Texture(Texture* aTexture); //copy constructor
	~Texture();

	int m_iImageWidth;
	int m_iImageHeight;
	int m_iImageFormat;

	unsigned int programID;
	unsigned int testMap;
	unsigned int DiffuseMap;
	unsigned int NormalMap;
	unsigned int DisplamenetMap;
	unsigned int SpecularMap;

	unsigned char m_data;
	std::string FragPath;
	std::string VertPath;
	
	unsigned int GetProgramID()
	{
		return programID;
	}

	void LoadTextures(const char* diffuseDir, const char* normalDir = nullptr, const char* displacementDir = nullptr, const char* specularDir = nullptr);
	//loads a shader program
	void LoadShaders(const char* a_vertShader, const char* a_fragShader);
	void DrawTexture();
	void Draw(glm::mat4 projectionView);
};

