#include "TextureLoader.h"

bool TextureLoader::LoadTexture(unsigned int* a_texture, const char * a_textureDir)
{
	int imageWidth = 0;
	int imageHeight = 0;
	int imageFormat = 0; 

	if (a_textureDir != nullptr) {
		unsigned char *data = stbi_load(a_textureDir, &imageWidth, &imageHeight, &imageFormat, STBI_default);
		glGenTextures(1, *&a_texture); //generate our texture
		glBindTexture(GL_TEXTURE_2D, *a_texture); //make sure it goes to the right position
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imageWidth, imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data); //load the texture data
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
		stbi_image_free(data);
		return true;
	}
	return false;

}

void TextureLoader::BindTextures(unsigned int a_programID, unsigned int a_diffuse, unsigned int a_normals, unsigned int a_specular)
{


	if (a_diffuse > 0) 
	{

		int loc = glGetUniformLocation(a_programID, "diffuse");
		glUniform1i(loc, 0);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, a_diffuse);
	}
	if (a_normals > 0)
	{
		int loc = glGetUniformLocation(a_programID, "normal");
		glUniform1i(loc, 1);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, a_normals);
	}

	if (a_specular > 0)
	{
		int loc = glGetUniformLocation(a_programID, "specular");
		glUniform1i(loc, 2);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, a_specular);
	}
}

unsigned int TextureLoader::LoadTexture(const char * a_textureDir)
{
	unsigned int texture;
	LoadTexture(&texture, a_textureDir);
	return texture;
}
