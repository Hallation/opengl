#pragma once
#include "stb\stb_image.h"
#include "gl_core_4_4.h"


class TextureLoader 
{
public:
	static bool LoadTexture(unsigned int* a_texture, const char * a_textureDir);
	static void BindTextures(unsigned int a_programID, unsigned int a_diffuse, unsigned int a_normals = 0, unsigned int a_specular = 0);
	static unsigned int LoadTexture(const char* a_textureDir);
};