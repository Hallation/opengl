class Timer
{
	Timer() {};

	Timer(float afWaitTime)
	{
		mfTimeToWait = afWaitTime;
		mfTimer = 0;
	}

	~Timer() {};

	bool Tick(float afIncrement)
	{
		mfTimer += afIncrement;
		if (mfTimer >= mfTimeToWait)
		{
			mfTimer = 0;
			return true;
		}
		return false;
	}

	bool WaitForEnd(float afIncrement)
	{
		while (mfTimer <= mfTimeToWait)
		{
			mfTimer += afIncrement;
		}
		return true;
	}
private:
	float mfTimeToWait;
	float mfTimer;
};