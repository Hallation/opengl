
//external libs
#include "gl_core_4_4.h"
#include "AIE\Gizmos.h"
#include <GLFW/glfw3.h>
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include "ProceduralApplication.h"
#include "AnimationApp.h"
#include "ParticleApplication.h"
#include "ClientApplication.h"
#include "LoginApplication.h"


using glm::vec3;
using glm::vec4;
using glm::mat4;

int main()
{
	srand((time(NULL)));
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//Application* mApplication = new Application();
	//ProceduralApplication* mApplication = new ProceduralApplication(); //completely broken
	//AnimationApp* mApplication = new AnimationApp(); //main application
	//ClientApplication* mApplication = new ClientApplication();
	LoginApplication* mApplication = new LoginApplication(); //client app to connect to server
	//mApplication->Startup();
	
	mApplication->Run();
	
	mApplication->Shutdown();
	delete mApplication;
	
	return 0;
}

