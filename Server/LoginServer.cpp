#include "LoginServer.h"
#include "GameMessages.h"


#include <DependentExtensions\SQLite3Plugin\sqlite3.h>
#include <cryptlib.h>
#include <osrng.h>
#include <hex.h>
#include <sha.h>

LoginServer::LoginServer()
{
	miMaxClients = 12;
	PORT = 7777;
	RCONPassword = "password123";
}


LoginServer::~LoginServer()
{
}


void LoginServer::InitializeDatase()
{
	//make a default database
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		//initialize sqlite3
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string createTableSQL = "CREATE TABLE IF NOT EXISTS user (userID INT PRIMARY KEY, username TEXT, password TEXT, userSalt TEXT, loginCount INT);";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, createTableSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare create table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to create table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}

		//if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, "INSERT IGNORE INTO user (userID, username, password, usersalt, loginCount) VALUES (00002, 'badusername', 'badpassword', 'badsalt', 1);", -1, &query, NULL)))
		//{
		//	printf("Failed to insert into table\n");
		//	break;
		//}
		//else if (sqlite3_step(query) != SQLITE_DONE)
		//{
		//	printf("Failed to insert into table %d, %s\n", ret, sqlite3_errmsg(pDB));
		//	break;
		//}

		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, "SELECT * FROM user;", -1, &query, NULL)))
		{
			printf("Failed to prepare select * from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (SQLITE_ROW != (ret = sqlite3_step(query)))
		{
			printf("Failed to count %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}

		for (int i = 0; i < sqlite3_column_count(query); ++i)
		{
			std::cout << sqlite3_column_name(query, i) << ": ";
			std::cout << sqlite3_column_text(query, i) << std::endl;
		}
		break;
	}
	if (NULL != query) sqlite3_finalize(query);
	if (NULL != pDB) sqlite3_close(pDB);
	sqlite3_shutdown();
}

void LoginServer::Insert(char * username, char * password)
{
	/*
	All checking to see if the data can be inserted has been processed before hand
	Before inserting, we need to hash the given password and require a salt to be generated;

	Count how many rows in db to know what our last ID is to get unique user IDS.
	From last user ID, using id formatting (0001....) iterate from last number
	Set this to be a new user entry.

	Select username from databse, if the username doesn't exist, they are good to go, otherwise reject anything further
	(probably do this before insert is ever used.
	Generate salt.
	Hash password with salt

	Insert hashed salted password into db

	insert salt into db
	*/
	int userID = (FindNextID() + 1);
	//username if it exists already done before enterting this method
	std::string salt = CreateSalt();
	std::string hashedPassword = HashPassword(password, salt);


	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, userID INT PRIMARY KEY, username TEXT, password TEXT, userSalt TEXT, loginCount INT)
		std::ostringstream sstream;
		//password is hashed with salt.
		sstream << "INSERT INTO USER (userID, username, password, userSalt) VALUES (" << userID << "," << username << "," << hashedPassword << "," << salt << ");";
		std::string sql = sstream.str();
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, sql.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare insert into table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to select item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		break;
	}
	if (NULL != query) sqlite3_finalize(query);
	if (NULL != pDB) sqlite3_close(pDB);
	sqlite3_shutdown();
}

bool LoginServer::AuthenticateUser(char * username, char * password)
{
	bool output = false;
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		//0 ID, 1 usr, 2 pwd, 3 salt, 4 count
		std::string selectSQL = "SELECT * FROM user WHERE username = " + std::string(username) + ";";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to select item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		//if there are actual records of the username
		std::cout << "Result count: " << sqlite3_column_count << std::endl;
		/*
		if (sqlite3_column_count > 0)
		{
			//get the user's salt from the query
			std::ostringstream saltstream;
			std::cout << sqlite3_column_text(query, 3);
			std::string salt = saltstream.str();
			std::string hashedIncomingPassword = HashPassword(password, salt);

			//get the password selected from the query
			std::ostringstream passwordstream;
			std::cout << sqlite3_column_text(query, 2);
			std::string databasepassword = passwordstream.str();
			std::string hashedDBpassword = HashPassword(databasepassword, salt);
			//commpare the 2 results
			//check to see if the client's password matches the one in the database
			if (hashedIncomingPassword == hashedDBpassword)
			{
				//Password has been accepted. move on
				output = true;
				break;
			}
			else
			{
				//password has been rejected. do something else.
				output = false;
				break; //break out of the loop
			}
		}
		else
		{
			//username does not exist
			output = false;
			break;
		}
		*/
	}
	//shutdown database
	if (NULL != query) sqlite3_finalize(query);
	if (NULL != pDB) sqlite3_close(pDB);
	sqlite3_shutdown();
	return output;
}

int LoginServer::FindNextID()
{
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string selectSQL = "SELECT * FROM user;";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to select item(s) from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		break;
	}
	int output = sqlite3_column_count(query);

	if (NULL != query) sqlite3_finalize(query);
	if (NULL != pDB) sqlite3_close(pDB);
	sqlite3_shutdown();

	return output;
}

bool LoginServer::FindUsername(char * username)
{
	sqlite3* pDB = NULL;
	sqlite3_stmt* query = NULL;
	int ret = 0;

	while (true)
	{
		if (SQLITE_OK != (ret = sqlite3_initialize()))
		{
			printf("Faled to initialize library: %d\n", ret);
		}
		//open a connection to a database
		if (SQLITE_OK != (ret = sqlite3_open_v2("test.db", &pDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL)))
		{
			printf("Failed to open conn: %d\n ", ret);
		}

		//compile SQL
		//create a user table with an ID, a username, password and login count
		//table structure, UserID PK, Username varchar(32), Password varchar(64), userSalt varchar(64), loginCount (INT);
		std::string selectSQL = "SELECT * FROM user WHERE username = " + std::string(username) + ";";
		if (SQLITE_OK != (ret = sqlite3_prepare_v2(pDB, selectSQL.c_str(), -1, &query, NULL)))
		{
			printf("Failed to prepare select table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		} //run
		else if (sqlite3_step(query) != SQLITE_DONE)
		{
			printf("Failed to select item from table: %d, %s\n", ret, sqlite3_errmsg(pDB));
			break;
		}
		break;
	}
	if (sqlite3_column_count > 0)
	{
		if (NULL != query) sqlite3_finalize(query);
		if (NULL != pDB) sqlite3_close(pDB);
		sqlite3_shutdown();
		return true;
	}
	if (NULL != query) sqlite3_finalize(query);
	if (NULL != pDB) sqlite3_close(pDB);
	sqlite3_shutdown();
	return false;
}

void LoginServer::handleNetworkMessages()
{
	std::cout << "Server started\n";
	RakNet::Packet* packet = nullptr;
	while (true)
	{
		for (packet = m_pPeerInterface->Receive(); packet; m_pPeerInterface->DeallocatePacket(packet), packet = m_pPeerInterface->Receive())
		{
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
				std::cout << "Connection incoming. \n";
				sendNewClientID(packet->systemAddress); //give my new client an ID
				break;
			case ID_DISCONNECTION_NOTIFICATION:
				//client initiated a disconnection
				onClientDisconnection(packet);
				m_pPeerInterface->CloseConnection(packet->systemAddress, false, 0, HIGH_PRIORITY);
				break;
			case ID_CONNECTION_LOST:
				//a packet couldnt be delivered so lose connection
				onClientTimeOut(packet->systemAddress);
				break;
			case ID_CLIENT_LOGIN:
				std::cout << "Login attemp incoming" << std::endl;
				onClientLogin(packet);
				break;
			case ID_CLIENT_REGISTER:
				onClientRegistration(packet);
				break;
			default:
				std::cout << "Unknown ID: " << packet->data[0] << std::endl;
				break;
			}
		}
	}
}

void LoginServer::onClientRegistration(RakNet::Packet * packet)
{
	User packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	//ignore bytes i dont want to read in
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData); //dump the packet data into the temp variable

	//do processing
	//First check for drop table shenanigans
	std::string username(packetData.username);
	std::string password(packetData.password);
	if (!username.find("DROP TABLE") > -1 && !password.find("DROP TABLE") > 1)
	{
		//if the username isnt found, it can be inserted.
		if (!FindUsername((char*)username.c_str()))
		{
			Insert((char*)username.c_str(), (char*)password.c_str());
		}
		else
		{
			//tell the client username already exists
			RakNet::BitStream bs;
			bs.Write((RakNet::MessageID)ID_USERNAME_EXISTS);
			m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
	}
	else
	{
		std::cout << "Found DROP TABLE No bamboozling here" << std::endl;
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_INVALID_USERPASS);
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
	}
}

void LoginServer::onClientLogin(RakNet::Packet * packet)
{
	User packetData;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(packetData);
	
	if (AuthenticateUser((char*)packetData.username, (char*)packetData.password))
	{ 
		std::cout << "Login attemp success" << std::endl;
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_USERPASS_ACCEPTED);
		m_pPeerInterface->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, false);
	}
	else
	{
		RakNet::BitStream bs;
		bs.Write((RakNet::MessageID)ID_INVALID_USERPASS);
		bs.Write("Invalid username or password");
		m_pPeerInterface->Send(&bs, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
		std::cout << "Login attempt failed" << std::endl;
	}
	
	

}

std::string LoginServer::CreateSalt()
{
	CryptoPP::SecByteBlock key(CryptoPP::AES::DEFAULT_KEYLENGTH);
	std::string salt;

	CryptoPP::OS_GenerateRandomBlock(true, key, key.size());

	CryptoPP::HexEncoder hex(new CryptoPP::StringSink(salt));
	hex.Put(key, key.size());
	hex.MessageEnd();

	return salt;
}

std::string LoginServer::HashPassword(std::string password, std::string salt)
{
	std::string saltedPassword = password + salt;

	byte const* pbData = (byte*)saltedPassword.data();
	unsigned int dataLength = saltedPassword.length();
	byte abDigest[CryptoPP::SHA256::DIGESTSIZE];

	CryptoPP::SHA256().CalculateDigest(abDigest, pbData, dataLength);

	std::string output;
	CryptoPP::HexEncoder hex(new CryptoPP::StringSink(output));
	hex.Put(abDigest, sizeof(abDigest));
	hex.MessageEnd();

	return output;
}

std::string LoginServer::HashPassword(char * password, std::string salt)
{
	return HashPassword(std::string(password), salt);
}
