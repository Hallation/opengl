
#include <MessageIdentifiers.h>
#include <RakPeerInterface.h>
#include <BitStream.h>

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include "GameMessages.h"
#include "Server.h"

void LookForInput(Server* aServer)
{
	std::string test;
	std::cin >> test;
	std::cout << test;
	aServer->mbRunning = false;
}

void main()
{
	srand((time(NULL)));
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//create and initalize my server
	Server* mServer = new Server();
	//Server* mServer = new Server();
	mServer->Initialize();
	mServer->SetLoginServerIP("127.0.0.1", 9999);
	
	std::thread inputWait(LookForInput, mServer);

	mServer->Run();

	inputWait.join();

	//t.join();
	//mServer->RunLoginServerConnection();
	//clear memory
	delete mServer;
	mServer = nullptr;

}
