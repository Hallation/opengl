#version 410
in vec2 vTexCoord;
in vec4 vNormal;

out vec4 fragColor;

uniform sampler2D diffuse;

void main() 
{
    //fragColor = vec4(vTexCoord.x, vTexCoord.y, 0,0);
    fragColor = texture(diffuse, vTexCoord);
};

