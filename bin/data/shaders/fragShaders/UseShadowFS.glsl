#version 410
in vec4 vNormal;
in vec4 vShadowCoord;

out vec4 fragColour;

uniform vec3 lightDir;
uniform sampler2D shadowMap;
uniform float shadowBias;

void main()
{
    float d = max(0, dot(normalize(vNormal.xyz), lightDir));

    if (texture(shadowMap, vShadowCoord.xy).r < vShadowCoord.z - shadowBias)
    {
        d = 0;
    }
    //fragColour = texture(shadowMap, vShadowCoord.xy);
    fragColour = vec4(d,d,d,1);
    //fragColour = vShadowCoord;
}