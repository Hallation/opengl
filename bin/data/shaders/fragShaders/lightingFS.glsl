#version 410

in vec2 vTexCoord;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBiTangent;

out vec4 fragColour;

uniform vec3 lightDir;
uniform vec4 lightColour;
uniform sampler2D diffuse;
uniform sampler2D normal;

void main()
{
    mat3 TBN = mat3 (normalize ( vTangent) , normalize( vBiTangent) , normalize( vNormal));
    vec3 N = texture(normal, vTexCoord).xyz * 2 - 1;

    float d  = max(0, dot(normalize (TBN * N), normalize(lightDir)));
    fragColour = texture(diffuse, vTexCoord);
    vec4 E = vec4(lightColour.xyz * d, 1);
    //fragColour.rgb = fragColour.rgb * d; + fragColour.rgb * E.xyz;
    fragColour.rgb = fragColour.rgb * d * lightColour.xyz;
}