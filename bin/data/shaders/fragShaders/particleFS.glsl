//FRAG SHADER
#version 410

in vec4 colour;
out vec4 fragColor;

void main()
{
    fragColor = colour;
}