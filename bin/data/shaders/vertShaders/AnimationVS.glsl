//VERT SHADER
#version 410
layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;

layout (location = 3) in vec3 Position2;
layout (location = 4) in vec3 Normal2;

out vec3 vNormal;
out vec2 vTexCoord;

uniform mat4 projectionView;
uniform vec4 mPosition;
uniform float keyTime;
void main ()
{
    vec3 p = mix(Position, Position2, keyTime);
    vec3 n = mix(Normal, Normal2, keyTime);

    vNormal = n;
    vTexCoord = TexCoord;
    
    gl_Position = projectionView * vec4(p, 1);
}