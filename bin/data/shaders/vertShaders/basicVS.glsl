//VERT SHADER
#version 410
layout (location = 0) in vec3 Position;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec2 TexCoord;

out vec3 vNormal;
out vec2 vTexCoord;

uniform mat4 projectionView;
uniform vec4 mPosition;
void main ()
{
    vTexCoord = TexCoord;
    vNormal = Normal;
    gl_Position = projectionView * (vec4(Position, 1) + mPosition);
}