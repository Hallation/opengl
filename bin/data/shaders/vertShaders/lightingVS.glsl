//VERT SHADER
#version 410

layout (location = 0) in vec4 positon;
layout (location = 1) in vec4 normal;
layout (location = 2) in vec2 texCoord;
layout (location = 3) in vec4 tangent;

out vec2 vTexCoord;
out vec3 vNormal;
out vec3 vTangent;
out vec3 vBiTangent;
out vec4 vPosition;
uniform mat4 projectionView;

void main()
{
    vPosition = positon;
    vTexCoord = texCoord;
    vNormal = normal.xyz;
    vTangent = tangent.xyz;
    vBiTangent = cross(vNormal, vTangent);
    gl_Position = projectionView * positon;
}