// VERT SHADER
#version 410
layout (location = 0)in vec4 Position;
layout (location = 1)in vec2 TexCoord;

uniform mat4 projectionView;

out vec2 vTexCoord;

uniform sampler2D perlinTexture;
void main()
{
    vec4 pos = Position;
    pos.y += texture(perlinTexture, TexCoord).r * 5;

    vTexCoord = TexCoord;
    gl_Position = projectionView * pos;
}