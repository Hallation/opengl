#pragma once

#include <MessageIdentifiers.h>

enum GameMessages
{
	ID_SERVER_TEXT_MESSAGE = ID_USER_PACKET_ENUM + 1,
	ID_SERVER_SET_CLIENT_ID,
	ID_CLIENT_CLIENT_DATA,
	ID_REMOTE_CONNECTED_CLIENTS,
	ID_CLIENT_SET_DATA,
	ID_CLIENT_LOGIN,
	ID_CLIENT_REGISTER,
	ID_USERNAME_EXISTS,
	ID_INVALID_USERPASS,
	ID_USERPASS_ACCEPTED,
	ID_SET_LOGIN_TOKEN,
	ID_CLIENT_TOKEN,
	ID_CLIENT_USERNAME,
	ID_INVALID_TOKEN,
	ID_SERVER_TOKEN_REQUEST,
	ID_SERVER_TOKEN_FOUND,
	ID_SERVER_TOKEN_NOT_FOUND,
	ID_LSERVER_ALREADY_LOGGED_IN,
	ID_CLIENT_LOG_OUT,
	ID_SERVER_CORRECT_TOKEN,
	ID_SERVER_INCORRECT_TOKEN,
	ID_MAIN_SET_ID,
	ID_SERVER_CLIENT_LOGOUT_NOTIFICATION,
	ID_CLIENT_USERNAME_LOGOUT,
};