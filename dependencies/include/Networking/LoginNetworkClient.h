#pragma once
#include "Structs.h"
#include "NetworkClient.h"

#include <vector>
#include <unordered_map>

class LoginNetworkClient :
	public NetworkClient
{
public:
	LoginNetworkClient();
	~LoginNetworkClient();

	void SetMainServer(char* IP, int PORT);
	void handleNetworkMessages();
	void ConnectToMain();
	void CheckForLogIn();
	void SetLoginIP(char* IP, int PORT);
	void onRegistrationAttempt(User a_user);
	void onLoginAttempt(User a_user);
	void onClientLogOut(RakNet::Packet* packet);
	void onRejectedRegistration(RakNet::Packet* packet);
	void onRejectedLogin(RakNet::Packet* packet);
	void onAcceptedLogin(RakNet::Packet* packet);
	void onAcceptedConnection(RakNet::Packet* packet);
	virtual void serverSetObject(); //tell the server my object
	virtual void OnReceivedClientDataPacket(RakNet::Packet* packet); //when receiving other peoples crap
	virtual void onReceivedClientConnections(RakNet::Packet* packet); //when another cleint connects
	virtual void onReceiveConnectedClients(RakNet::Packet* packet); //this loads any previously logged in players
	virtual void onRemoteDisconnection(RakNet::Packet* packet); //whenever a user disconnects

	void LogOut();
	void DisconnectLogOut();
	void UpdateObject();
	bool IsLoggedIn();
	bool ConnectedToMain();
	void ConnectToLogin();
	User mUser;

	bool LoggedIn;
	bool mbConnectedToMain;

	std::unordered_map<int, GameObject>mClientGameObjects;
	GameObject mGameObject;
private:

	LoginToken mLoginInfo;
	
	char* LoginIP;
	int loginPORT;
	/* 
	bool mbConnected;
	RakNet::RakPeerInterface* m_pPeerInterface;
	unsigned int miClientID;
	char* IP;
	int PORT;
	*/
};

