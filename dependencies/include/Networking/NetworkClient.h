#pragma once

//#include "Structs.h"
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <unordered_map>


class NetworkClient
{
public:
	NetworkClient();
	~NetworkClient();

	void handleNetworkConnection();
	void initalizeClientConnection();
	void Disconnect();
	void onSetClientIDPacket(RakNet::Packet* packet);
	void CreateConnection(char* ipAddress, int Port);

	//handle incoming packets
	virtual void handleNetworkMessages();
	virtual void serverSetObject();
	virtual void OnReceivedClientDataPacket(RakNet::Packet* packet);
	virtual void onReceivedClientConnections(RakNet::Packet* packet);
	virtual void onReceiveConnectedClients(RakNet::Packet* packet);
	virtual void onRemoteDisconnection(RakNet::Packet* packet);
	virtual void onLostConnection(RakNet::Packet* packet);
	virtual void onDisconnection();
	void Run();

	RakNet::RakPeerInterface* getInterface()
	{
		return m_pPeerInterface;
	}
	unsigned int getClientID()
	{
		return miClientID;
	}
	bool Connected()
	{
		return mbConnected;
	}

	void SetConnectionLocation(char* aIPAddress, short aPORT);

	int miClientID = -1;
private:


protected:
	bool mbConnected;
	RakNet::RakPeerInterface* m_pPeerInterface;
	char* IP;
	int PORT;
};

