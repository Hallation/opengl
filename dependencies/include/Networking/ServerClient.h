#pragma once
#include "NetworkClient.h"
class ServerClient :
	public NetworkClient
{
	//class is used on servers for a connection to a different server
public:
	ServerClient();
	~ServerClient();

	void SetServer(char* IP, int PORT);
	void handleNetworkMessages();

	void Run();

private:

	bool LoggedIn;
	bool ConnectedToMain;
};

